
from django import forms

CATEGORY_CHOICES= [
    ('Trading Card Game', 'Trading Card Game'),
    ('Gambling Game', 'Gambling Game'),
    ('Weaboo Stuff', 'Weaboo Stuff'),
    ('Dolls and Action Figures', 'Dolls and Action Figures'),
    ('Adult Toys', 'Adult Toys'),
]

class ItemRegistrationForm(forms.Form):
    nama = forms.CharField(max_length=255, required=True, label='Item Name')
    deskripsi = forms.CharField(max_length=255, label='Item Description')
    usia_dari = forms.IntegerField(required=True, label='Min. Age')
    usia_sampai = forms.IntegerField(required=True, label='Max. Age')
    bahan = forms.CharField(max_length=255, label='Item Material')
    kategori = forms.CharField(label='Item Category', widget=forms.Select(choices=CATEGORY_CHOICES))