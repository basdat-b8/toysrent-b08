import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt

from .forms import ItemRegistrationForm

class ItemListPage(View):
    page_file  = "item_list.html"
    def get(self, request):
        temp = ItemList()
        data = temp.get_items()
        response = {
            "data" : data
        }
        return render(request, self.page_file, response)

class ItemAddPage(View):    
    def get(self, request):
        page_file = "item_add.html"
        response = {
            "item_registration_form" : ItemRegistrationForm
        }
        return render(request, page_file, response)

class ItemList(View):
    def get_items(self):
        json_req = requests.get("http://localhost:8000/api/items/all")
        json_data = json_req.json()
        for element in json_data:
            raw_id = element['nama'].lower()
            new_id = ("-").join(raw_id.split())
            element['item_id'] = new_id
        return json_data
    
class AddItemAction(View):
    @csrf_exempt       
    def post(self, request):  
        data_to_send = {
            "nama" : request.POST['nama'],
            "deskripsi" : request.POST['deskripsi'],
            "usia_dari" : request.POST['usia_dari'],
            "usia_sampai" : request.POST['usia_sampai'],
            "bahan" : request.POST['bahan'],
            "kategori" : request.POST['kategori'],
        }
        requests.post("http://localhost:8000/api/items/register/", data=data_to_send)
        data = { "item_added" : 1 }
        return JsonResponse(data)

class DeleteItemAction(View):
    @csrf_exempt
    def get(self, request, item_id):
        requests.delete("http://localhost:8000/api/items/" + item_id)
        return redirect('/items/')