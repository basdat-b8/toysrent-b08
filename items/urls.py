from django.urls import path
from .views import(
    ItemListPage, ItemAddPage, AddItemAction, DeleteItemAction
)

app_name = "items"

urlpatterns = [
	path('', ItemListPage.as_view(), name="item_list"),
    path('add/', ItemAddPage.as_view(), name="item_add"),
    path('add/begin/', AddItemAction.as_view(), name="add_item_action"),
    path('delete/<str:item_id>/', DeleteItemAction.as_view(), name="delete_item_action"),
]