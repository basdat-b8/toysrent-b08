import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt

class ProductListPage(View):
    page_file  = "product_list.html"
    def get(self, request):
        temp = ProductList()
        data = temp.get_products()
        response = {
            "data" : data
        }
        return render(request, self.page_file, response)

# def product_detail(request, nama_item):
#     page_file = "product_detail.html"
#     json_req = requests.get("http://localhost:8000/api/items/" + nama_item)
#     json_data = json_req.json()
#     response = {
#         "data" : json_data
#     }
#     return render(request, page_file, response)

class ProductAddPage(View):
    page_file = "product_add.html"
    def get(self, request):
        return render(request, self.page_file)

class ProductList(View):
    def get_products(self):
        json_req = requests.get("http://localhost:8000/api/products/all")
        json_data = json_req.json()
        return json_data

def deleteProduct(request, product_id):
    requests.delete("http://localhost:8000/api/products/" + product_id)
    return redirect("/products/")