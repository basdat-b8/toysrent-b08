from django.urls import path
from .views import(
    ProductListPage, ProductAddPage
)

from .views import deleteProduct

app_name = "products"

urlpatterns = [
	path('', ProductListPage.as_view(), name="product_list"),
    path('add/', ProductAddPage.as_view(), name="product_add"),
    # path("detail/", product_detail, name="product_detail"),
    path('delete/<str:product_id>/', deleteProduct)
]