$(document).ready(function () {
    secureAJAX();
});

csrfSafeMethod = (method) => {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
secureAJAX = () => {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

do_register_user = (user_role) => {
    var no_ktp = $('#id_no_ktp').val();
    var nama_lengkap = $('#id_nama_lengkap').val();
    var email = $('#id_email').val();
    var tanggal_lahir = $('#id_tanggal_lahir').val();
    var no_telp = $('#id_no_telp').val();

    if (no_ktp !== '' && nama_lengkap !== '' && email !== '') {
        alert("OK");
    }
}

do_login_user = () => {
    var no_ktp = $('#id_no_ktp').val();
    var email = $('#id_email').val();
    if (no_ktp !== '' && email !== '') {
        showOnlyWaitCard()
        $.post("begin/", {
            "no_ktp": no_ktp,
            "email": email,
        }, "json")
            .done((data) => {
                if (data.registered_status) {
                    successBox("Login Successful! Please Wait..");
                    if (data.role == 'anggota') window.location.replace("/products/");
                    else window.location.replace("/orders/");
                } else {
                    errorBox("User Does Not Exists! Please Try Again");
                }
            })
            .fail((data) => {
                errorBox("Login Failed! Please Check Connection");
            });
    } else {
        alert("Please fill in all the boxes before continue");
    }
}

showOnlyWaitCard = () => {
    if ($("#successCard").hasClass("d-none")) {
        $("#errorCard").addClass("d-none");
    } else {
        $("#successCard").addClass("d-none");
    }
    $("#waitCard").removeClass("d-none");
}

errorBox = (error_message) => {
    $("#waitCard").addClass("d-none");
    $("#successCard").addClass("d-none");
    $("#errorCard").removeClass("d-none");
    $("#error").text(error_message);
}

successBox = (success_message) => {
    $("#waitCard").addClass("d-none");
    $("#errorCard").addClass("d-none");
    $("#successCard").removeClass("d-none");
    $("#success").text(success_message);
}