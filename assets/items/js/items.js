$(document).ready(function () {
    secureAJAX();
});

csrfSafeMethod = (method) => {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
secureAJAX = () => {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

do_add_item = () => {
    var nama = $('#id_nama').val();
    var deskripsi = $('#id_deskripsi').val();
    var usia_dari = $('#id_usia_dari').val();
    var usia_sampai = $('#id_usia_sampai').val();
    var bahan = $('#id_bahan').val();
    var kategori = $('#id_kategori').val();

    if (nama !== '' && usia_dari !== '' & usia_sampai !== '') {
        showOnlyWaitCard()
        $.post("begin/", {
            "nama": nama,
            "deskripsi": deskripsi,
            "usia_dari": usia_dari,
            "usia_sampai": usia_sampai,
            "bahan": bahan,
            "kategori": kategori,
        }, "json")
            .done((data) => {
                successBox("Item Added Successfully!");
                window.location.replace("/items/");
            })
            .fail((data) => {
                errorBox("Action Failed! Please Check Connection");
            });
    } else {
        alert("Please fill in the boxes before continue");
    }
}

do_update_item = () => {


}

do_delete_item = (item_id) => {
    $.post("delete/", {
        "item_id": item_id
    }, "json")
        .done((data) => {
            window.location.replace("/items/");
        })
        .fail((data) => {
            alert("Delete Failed! Please Check Connection");
        });
}

showOnlyWaitCard = () => {
    if ($("#successCard").hasClass("d-none")) {
        $("#errorCard").addClass("d-none");
    } else {
        $("#successCard").addClass("d-none");
    }
    $("#waitCard").removeClass("d-none");
}

errorBox = (error_message) => {
    $("#waitCard").addClass("d-none");
    $("#successCard").addClass("d-none");
    $("#errorCard").removeClass("d-none");
    $("#error").text(error_message);
}

successBox = (success_message) => {
    $("#waitCard").addClass("d-none");
    $("#errorCard").addClass("d-none");
    $("#successCard").removeClass("d-none");
    $("#success").text(success_message);
}