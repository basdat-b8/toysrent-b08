$(document).ready(function () {
    secureAJAX();
});

csrfSafeMethod = (method) => {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
secureAJAX = () => {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

do_add_level = () => {
    var nama_level = $('#id_nama_level').val();
    var min_poin = $('#id_min_poin').val();
    var deskripsi = $('#id_deskripsi').val();

    if (nama_level !== '' && min_poin !== '' & deskripsi !== '') {
        showOnlyWaitCard()
        $.post("begin/", {
            "nama_level": nama_level,
            "min_poin": min_poin,
            "deskripsi": deskripsi,
        }, "json")
            .done((data) => {
                successBox("Level Added Successfully!");
                window.location.replace("/level/");
            })
            .fail((data) => {
                errorBox("Action Failed! Please Check Connection");
            });
    } else {
        alert("Please fill in the boxes before continue");
    }
}

do_delete_item = (nama_level) => {
    $.post("delete/", {
        "nama_level": nama_level
    }, "json")
        .done((data) => {
            window.location.replace("/level/daftar_level");
        })
        .fail((data) => {
            alert("Delete Failed! Please Check Connection");
        });
}

showOnlyWaitCard = () => {
    if ($("#successCard").hasClass("d-none")) {
        $("#errorCard").addClass("d-none");
    } else {
        $("#successCard").addClass("d-none");
    }
    $("#waitCard").removeClass("d-none");
}

errorBox = (error_message) => {
    $("#waitCard").addClass("d-none");
    $("#successCard").addClass("d-none");
    $("#errorCard").removeClass("d-none");
    $("#error").text(error_message);
}

successBox = (success_message) => {
    $("#waitCard").addClass("d-none");
    $("#errorCard").addClass("d-none");
    $("#successCard").removeClass("d-none");
    $("#success").text(success_message);
}