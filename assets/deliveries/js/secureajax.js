$(document).ready(function() {
	secureAJAX();
});

csrfSafeMethod = (method) => { return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method)); }
secureAJAX = () =>{
	var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});
}