

do_delete_delivery = (item_id) => {
	$.post( "delete/", {
		"item_id" : item_id
	},"json")
	.done((data)=>{
		window.location.replace("/items/");
	})
	.fail((data)=>{
		alert("Delete Failed! Please Check Connection");
	});
}


// Pagination
$(document).ready(function() {
    $('#list_pengiriman_body').pageMe({
    pagerSelector: '#developer_page',
    showPrevNext: true,
    hidePageNumbers: false,
    perPage: 10
    });
    });


// Forms
do_add_item = () => {
	var nama = $('#id_nama').val();
	var deskripsi = $('#id_deskripsi').val();
	var usia_dari = $('#id_usia_dari').val();
	var usia_sampai = $('#id_usia_sampai').val();
	var bahan = $('#id_bahan').val();
	var kategori = $('#id_kategori').val();

	if(nama !== '' && usia_dari !== '' & usia_sampai !== ''){
		showOnlyWaitCard()
		$.post( "begin/", {
			"nama": nama,
			"deskripsi": deskripsi,
			"usia_dari": usia_dari,
			"usia_sampai": usia_sampai,
			"bahan": bahan,
			"kategori": kategori,
		},"json")
		.done((data)=>{
			successBox("Item Added Successfully!");
			window.location.replace("/items/");
		})
		.fail((data)=>{
			errorBox("Action Failed! Please Check Connection");
		});
	}
	else{
		alert("Please fill in the boxes before continue");
	}
}

