import json
import requests

from django.shortcuts import render

# Create your views here.
def chats(request):
    page = "chats.html"
    chatData = getChatData()
    response = {
        "data": chatData
    }
    return render(request, page, response)

def getChatData():
    json_req = requests.get("http://localhost:8000/api/chats/all/")
    json_data = json_req.json()
    return json_data