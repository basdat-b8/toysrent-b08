from django.urls import path
from .views import chats

urlpatterns = [
    path("", chats),
]