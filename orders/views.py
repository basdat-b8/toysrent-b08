import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt

# class OrderListPage(View):
#     page_file  = "order_list.html"
#     def get(self, request):
#         return render(request, self.page_file)

class OrderAddPage(View):
    page_file = "order_add.html"
    def get(self, request):
        return render(request, self.page_file)


def daftar_pesanan(request):
    response = {}
    response['activetab'] = 'daftarpesanan'
    page = "order_list.html"
    data = getPesananData()
    context = {
        "data" : data
    }
    return render(request, page, context)

def getPesananData():
    #ini link apinya ubah
    json_req = requests.get("http://localhost:8000/api/orders/all/")
    json_data = json_req.json()
    return json_data