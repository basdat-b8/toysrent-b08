from django.urls import path
from .views import(
    daftar_pesanan, OrderAddPage, 
)

app_name = "orders"

urlpatterns = [
	# path('', OrderListPage.as_view(), name="order_list"),
    path('list/', daftar_pesanan, name = "daftar_pesanan"),
    path('add/', OrderAddPage.as_view(), name="order_add"),
]