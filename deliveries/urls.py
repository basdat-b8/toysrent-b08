from django.urls import path
from deliveries import views

app_name = "deliveries"

urlpatterns = [
    path('list/', views.daftar_pengiriman, name='daftarPengiriman'),
    path('add/', views.daftar_pengiriman_add, name="delivery_add"),
    path('delete/<str:no_resi>/', views.deleteitemaction, name="delete_delivery_action"),
]