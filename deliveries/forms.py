from django import forms

ADDRESS_CHOICES= [
    ('A1', 'A1'),
    ('A2', 'A2'),
]

ITEMS_CHOICES= [
    ('A1', 'A1'),
    ('A2', 'A2'),
]

class DeliveryRegistrationForm(forms.Form):
    barang_pesanan = forms.CharField(label='Ordered Items', widget=forms.SelectMultiple(choices=ITEMS_CHOICES))
    alamat_tujuan = forms.CharField(label='Destination Address', widget=forms.Select(choices=ADDRESS_CHOICES))
    tanggal_pengiriman = forms.DateField(label='Tanggal Pengiriman')
    metode = forms.CharField(max_length=255, required=True, label='Item Name')