import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
from deliveries.forms import DeliveryRegistrationForm


# Create your views here.
def daftar_pengiriman(request):
    response = {}
    response['activetab'] = 'daftarpengiriman'
    page = "list_pengiriman.html"
    data = getDeliveryData()
    context = {
        "data" : data
    }
    return render(request, page, context)

def getDeliveryData():
    # ini link apinya ubah
    json_barang_dikirim_req = requests.get("http://localhost:8000/api/products/delivery/all/")
    json_barang_req = requests.get("http://localhost:8000/api/products/all/")
    json_pengiriman_req = requests.get("http://localhost:8000/api/delivery/all") 
    json_barang_dikirim = json_barang_dikirim_req.json()
    json_barang = json_barang_req.json()
    json_pengiriman_data = json_pengiriman_req.json()
    for each_barang_dikirim in json_barang_dikirim:
        for each_barang in json_barang:
            if each_barang_dikirim["id_barang"] == each_barang["id_barang"]:
                each_barang_dikirim["nama_item"] = each_barang["nama_item"]
    for each_pengiriman in json_pengiriman_data:
        each_pengiriman["nama_item"] = []
        for each_barang_dikirim in json_barang_dikirim:
            if each_pengiriman["no_resi"] == each_barang_dikirim["no_resi"]:
                each_pengiriman["nama_item"].append(each_barang_dikirim["nama_item"])
                print(each_pengiriman["nama_item"])
    return json_pengiriman_data

#Untuk Delivery Form
def daftar_pengiriman_add(request):
    page_file = "pengiriman_add.html"
    context = {
        "delivery_registration_form" : DeliveryRegistrationForm
    }
    return render(request, page_file, context)

@csrf_exempt       
def additemaction(request):  
    data_to_send = {
        "barang_pesanan" : request.POST['barang_pesanan'],
        "alamat_tujuan" : request.POST['alamat_tujuan'],
        "tanggal_pengiriman" : request.POST['tanggal_pengiriman'],
        "metode" : request.POST['metode'],
    }
    requests.post("http://localhost:8000/api/deliveries/register/", data=data_to_send)
    data = { "item_added" : 1 }
    return JsonResponse(data)
    
@csrf_exempt
def deleteitemaction(request, no_resi):
    requests.delete("http://localhost:8000/api/delivery/" + no_resi)
    return redirect('/../deliveries/list/')


