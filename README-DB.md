## Endpoint untuk Database Toys Rent  

Pemanggilan data dari database Toys Rent cukup mudah!
Anda cukup menembak URL dengan tipe request yang telah ditentukan!  
Cara penembakannya sendiri sudah diajarkan di PPW
 
 Daftar API sudah disediakan di sini:
 [https://toysrent-b8.herokuapp.com/api/](https://toysrent-b8.herokuapp.com/api/)

> Setiap GET memiliki tujuan untuk mengambil data dari database .

Setiap operasi GET yang dilakukan akan mengembalikan data berupa list/array of JSON yang berisikan semua atribut + value dari tabel di database, apabila data tidak ditemukan, akan mengembalikan status code 404.

Saat operasi GET dilakukan, tidak perlu ada ada JSON yang dikirimkan, cukup panggil alamatnya saja, dan terima hasilnya

Contoh:
(GET) https://toysrent-b8.herokuapp.com/api/users/all/ akan mengembalikan list/array of JSON pengguna (admin & anggota)

> Setiap POST memiliki tujuan untuk menaruh data ke database .

Setiap operasi POST yang dilakukan TIDAK akan mengembalikan data, melainkan hanya mengembalikan status code 200 (Sukses), 500 (Kesalahan dalam pengolahan data seperti urutan JSON tidak sesuai, atau query yang salah). operasi POST ini juga telah didukung untuk melakukan proses INSERT atau UPDATE pada database, untuk melakukan UPDATE cukup dengan melakukan POST ulang data yang hendak diubah.

Saat operasi POST dilakukan, harus mengirimkan juga JSON dari data yang hendak diubah

Contoh:
(POST) https://toysrent-b8.herokuapp.com/api/users/register/ akan memasukkan/update JSON dari pengguna ke dalam tabel di database

> Setiap DELETE memiliki tujuan untuk menghapus data dari database .

Sama halnya dengan POST, setiap operasi DELETE yang dilakukan TIDAK akan mengembalikan data, melainkan hanya mengembalikan status code 200 (Sukses), 500 (Kesalahan dalam pengolahan data seperti urutan JSON tidak sesuai, atau query yang salah).

Saat operasi DELETE dilakukan, tidak perlu ada ada JSON yang dikirimkan, cukup panggil alamatnya saja, dan terima hasilnya . Harap diperhatikan beberapa alamat untuk DELETE sedikit berbeda dikarenakan ada perbedaan handle tuple Primary Key pada beberapa tabel pada database

Contoh:
(DELETE) https://toysrent-b8.herokuapp.com/api/users/USER001/ akan menghapus pengguna dengan id USER001 dari tabel di database
```  
(GET) api/users/all/ => Return semua data pengguna  
(POST) api/users/register/ => Memasukkan data ke dalam database  
(GET) api/users/<user_id>/ => Return data pengguna spesifik berdasarkan id  
(DELETE) api/users/<user_id>/ => Menghapus data pengguna berdasarkan id  
  
(GET) api/admins/all/ => Return semua data admin  
(POST) api/admins/register/ => Memasukkan data admin ke dalam database  
(DELETE) api/admins/<user_id>/ => Menghapus data admin berdasarkan id  
  
(GET) api/members/all/ => Return semua data member  
(POST) api/members/register/ => Memasukkan data member ke dalam database  
(GET) api/members/<user_id>/ => Return data member spesifik berdasarkan id  
(DELETE) api/members/<user_id>/ => Menghapus data member berdasarkan id  
  
(GET) api/levels/all/ => Return semua data level keanggotaan  
(POST) api/levels/register/ => Memasukkan data ke dalam database  
(GET) api/levels/<nama_level>/ => Return data level spesifik berdasarkan nama level  (Setiap kata dipisah dengan tanda -)
(DELETE) api/levels/<nama_level> => Menghapus data dari database berdasarkan nama level (Setiap kata dipisah dengan tanda -)
  
(GET) api/items/all/ => Return semua item  
(POST) api/items/register/ => Memasukkan data ke database  
(GET) api/items/<nama_item>/ => Return data item spesifik berdasarkan id  
(DELETE) api/items/<nama_item> => Menghapus data dari database berdasarkan nama item  
  
(GET) api/address/all/ => Return semua alamat pengguna  
(POST) api/address/register/ => Memasukkan data ke database  
(GET) api/address/<no_ktp_anggota>/ => Return data alamat spesifik berdasarkan nomor KTP anggota  
(DELETE) api/address/<no_ktp_anggota> => Menghapus data dari database berdasarkan nomor KTP anggota  
  
(GET) api/products/all/ => Return semua barang  
(POST) api/products/register/ => Memasukkan barang ke database  
(GET) api/products/<id_barang>/ => Return data barang spesifik berdasarkan id barang  
(DELETE) api/products/<id_barang> => Menghapus data dari database berdasarkan id barang  
  
(GET) api/products/return/all/ => Return semua barang yang dikembalikan  
(POST) api/products/return/register/ => Memasukkan barang yang dikembalikan ke database  
(GET) api/products/return/<id_barang>/ => Return data barang yang dikembalikan spesifik berdasarkan id barang  
(DELETE) api/products/return/<id_barang> => Menghapus data dari database berdasarkan id barang yang dikembalikan  
  
(GET) api/products/delivery/all/ => Return semua barang yang sedang dalam pengiriman  
(POST) api/products/delivery/register/ => Memasukkan barang yang dikirim ke database  
(GET) api/products/delivery/<no_resi>/ => Return barang yang dikembalikan berdasarkan no_resi (Catatan: 1 resi bisa memiliki >1 barang)  
(DELETE) api/products/delivery/<no_resi>/<no_urutan>/ => Menghapus data dari database berdasarkan kombinasi PK no_resi dan no_urutan  
  
(GET) api/products/order/all/ => Return semua barang yang dipesan  
(POST) api/products/order/register/ => Memasukkan barang yang dipesan ke database  
(GET) api/products/order/<id_pemesanan>/ => Return barang yang dipesan berdasarkan id_pemesanan (Catatan: 1 id_pemesanan bisa memiliki >1 barang)  
(DELETE) api/products/order/<id_pemesanan>/<no_urutan>/ => Menghapus data dari database berdasarkan kombinasi PK id_pemesanan dan no_urutan 

(GET) api/chats/all/ => Return semua chat  
(POST) api/chats/register/ => Memasukkan chat ke database  
(GET) api/chats/<id>/ => Return data chat spesifik berdasarkan id chat  
(DELETE) api/chats/<id> => Menghapus data dari database berdasarkan id chat  
 
(GET) api/products/level/all/ => Return semua informasi level barang
(POST) api/products/level/register/ => Memasukkan informasi level barang ke database
(GET) api/products/level/<id_barang>/ => Return data informasi level barang spesifik berdasarkan id barang
(DELETE) api/products/level/<id_barang>/<nama_level>/ => Menghapus data dari database berdasarkan kombinasi PK id_barang dan nama_level

(GET) api/categories/all/ => Return semua data kategori
(POST) api/categories/register/ => Memasukkan data kategori ke dalam database
(GET) api/categories/<nama>/ => Return data kategori spesifik berdasarkan nama kategori (Setiap kata dipisah dengan tanda -)
(DELETE) api/categories/<nama>/ => Menghapus data dari database berdasarkan nama kategori (Setiap kata dipisah dengan tanda -)

(GET) api/items/category/all/ => Return semua informasi kategori item
(POST) api/items/category/register/ => Memasukkan informasi kategori item ke database
(GET) api/items/category/<nama_item>/ => Return data informasi kategori item spesifik berdasarkan nama item
(DELETE) api/items/category/<nama_item>/<nama_kategori>/ => Menghapus data dari database berdasarkan kombinasi PK nama_item dan nama_kategori

(GET) api/orders/all/ => Return semua pemesanan
(POST) api/orders/register/ => Memasukkan pemesanan ke database  
(GET) api/orders/<id>/ => Return data pemesanan spesifik berdasarkan id pemesanan
(DELETE) api/orders/<id> => Menghapus data dari database berdasarkan id pemesanan  

(GET) api/returns/all/ => Return semua pengembalian
(POST) api/returns/register/ => Memasukkan pengembalian ke database  
(GET) api/returns/<id>/ => Return data pengembalian spesifik berdasarkan id pengembalian
(DELETE) api/returns/<id> => Menghapus data dari database berdasarkan id pengembalian

```