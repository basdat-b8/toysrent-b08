"""toys_rent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from level import views

urlpatterns = [
    path('', views.level, name='level'),
    path('daftar_level/', views.daftar_level, name='daftarlevel'),
    path('daftar_level/update_level', views.updatelevel, name='updatelevel'),
    path('daftar_level/delete/<str:nama_level>/', views.DeleteLevel, name="delete_level"),
]