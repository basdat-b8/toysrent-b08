
from django import forms

class TambahLevel(forms.Form):
    nama_level = forms.CharField(max_length=255, required=True, label='Level Name')
    min_poin = forms.CharField(max_length=255, required=True, label='Minimum Poin')
    deskripsi = forms.CharField(max_length=255, required=True, label='Description')