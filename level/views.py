import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt

from .forms import TambahLevel

# Create your views here.
def level(request):
    response = {}
    return render(request, "level.html")

# def daftar_level(request):
#     response = {}
#     response['activetab'] = 'daftar_level'
#     return render(request, "daftar_level.html", response)

def updatelevel(request):
    response = {}
    return render(request,"updatelevel.html", response)

def daftar_level(request):
    page = "daftar_level.html"
    data = getLevelData()
    context = {
        "data" : data
    }
    return render(request, page, context)

def getLevelData():
    json_req = requests.get("http://localhost:8000/api/levels/all")
    json_data = json_req.json()
    for element in json_data:
            raw_id = element['nama_level'].lower()
            new_id = ("-").join(raw_id.split())
            element['nama_level'] = new_id
    return json_data

def DeleteLevel(request, nama_level):
        requests.delete("http://localhost:8000/api/level/" + nama_level)
        return redirect('/level/daftar_level')