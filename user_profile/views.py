import json
import requests

from django.views import View
from django.shortcuts import render, redirect

class ProfilePage(View):
    page_file  = "profile.html"
    def get(self, request):
        return render(request, self.page_file)