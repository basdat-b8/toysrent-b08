from django.urls import path
from .views import(
ProfilePage
)

app_name = "user_profile"

urlpatterns = [
	path('', ProfilePage.as_view(), name="login_page"),
]