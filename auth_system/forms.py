from django import forms

class UserRegistrationForm(forms.Form):
    no_ktp = forms.CharField(max_length=20, required=True, label='Identity Number')
    nama_lengkap = forms.CharField(max_length=255, required=True, label='Full Name')
    email = forms.CharField(max_length=255, required=True, label='E-Mail')
    tanggal_lahir = forms.DateField(required=True,
                                    input_formats=['%Y-%m-%d'],
                                    label='Date of Birth')
    no_telp = forms.CharField(max_length=20, label='Phone Number')

class AddressForm(forms.Form):
    nama = forms.CharField(max_length=255, required=True, label='Name')
    jalan = forms.CharField(max_length=255, required=True, label='Street')
    nomor = forms.IntegerField(required=True, label="Number")
    kota = forms.CharField(max_length=255, required=True, label='City')
    kodepos = forms.CharField(max_length=10, required=True, label='Zip Code')

class UserLoginForm(forms.Form):
    no_ktp = forms.CharField(max_length=20, required=True, label='Identity Number')
    email = forms.CharField(max_length=255, required=True, label='E-Mail Address')