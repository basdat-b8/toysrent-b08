from django.urls import path
from .views import(
	LoginPage, RegistrationPage, Logout,
	LoginAction
)

app_name = "auth_system"

urlpatterns = [
	path('login/', LoginPage.as_view(), name="login_page"),
    path('login/begin/', LoginAction.as_view(), name="login_action"),
	path('register/<str:role>/', RegistrationPage.as_view(), name="registration_page"),
    path('logout/', Logout.as_view(), name="logout"),
]