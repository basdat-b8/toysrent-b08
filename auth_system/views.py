import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt

from .forms import UserLoginForm, UserRegistrationForm, AddressForm

class RegistrationPage(View):
    page_file  = "signup.html"
    def get(self, request, role):
        response = {
            "registration_form" : UserRegistrationForm,
            "address_form" : AddressForm,
            "role" : role
        }
        return render(request, self.page_file, response)

class LoginPage(View):
    page_file = "login.html"
    def get(self, request):
        response = {
            "login_form" : UserLoginForm
        }
        return render(request, self.page_file, response)

class Logout(View):
    def get(self,request):
        request.session.flush()
        return redirect("/")

class UserRegistration(View):
    def post(self, request):
        pass

class LoginAction(View):
    @csrf_exempt
    def post(self, request):        
        no_ktp = request.POST['no_ktp']
        email = request.POST['email']

        json_req = requests.get("http://localhost:8000/api/users/all")
        json_data = json_req.json()
        user_registered = False
        user_role = 'anggota'
        for element in json_data:
            if element['no_ktp'] == no_ktp and element['email'] == email:
                user_registered = True
                if int(no_ktp[4:]) % 10 == 0 :
                    user_role = 'admin'
                self.save_session(request, element, user_role)
                break
        data = {
            "registered_status" : user_registered,
            "role" : user_role,
        }
        return JsonResponse(data);
    
    def save_session(self, request, data, user_role):
        request.session['user'] = data['no_ktp']
        request.session['nama_lengkap'] = data['nama_lengkap']
        request.session['email'] = data['email']
        request.session['tanggal_lahir'] = data['tanggal_lahir']
        request.session['no_telp'] = data['no_telp']
        request.session['role'] = user_role
        if user_role == 'anggota':
            request.session['alamat'] = self.get_member_address(data['no_ktp'])
            point_level = self.get_member_point_level(data['no_ktp'])
            request.session['poin'] = point_level['poin']
            request.session['level'] = point_level['level']

    def get_member_address(self, no_ktp):
        json_req = requests.get("http://localhost:8000/api/address/" + no_ktp)
        json_data = json_req.json()
        address = ""
        address += (
            json_data['nama']
            + ", "
            + json_data['jalan']
            + "street "
            + "no " + str(json_data['nomor']) + ". "
            + json_data['kota'] + "city, "
            + json_data['kodepos']
        )
        return address
    
    def get_member_point_level(self, no_ktp):
        json_req = requests.get("http://localhost:8000/api/members/" + no_ktp)
        json_data = json_req.json()
        data = {
            "poin" : json_data['poin'],
            "level" : json_data['level']
        }
        return data
