from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('', include('home.urls')),
    path('auth/', include('auth_system.urls')),
    path('profile/', include('user_profile.urls')),
    path('products/', include('products.urls')),
    path('orders/', include('orders.urls')),
    path('items/', include('items.urls')),
    path('level/', include('level.urls')),
    path('chats/', include('chats.urls')),
    path('deliveries/', include('deliveries.urls')),
    path('reviews/', include('review.urls')),
    
    # path('detail_barang/', include('daftar_detail_barang.urls')),
    # path('level/', include('level.urls')),
    # path('pesanan/', include('pesanan.urls')),
]
