from django.urls import path
from home import views
from .views import HomePage

app_name = 'home-page'
urlpatterns = [
	path('', HomePage.as_view(), name='home-page')
]
