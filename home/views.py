import json
import requests
from django.views import View
from django.shortcuts import render

class HomePage(View):
    page_file  = "home_page.html"

    def get(self, request):
        return render(request, self.page_file)