
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)  [![pipeline status](https://gitlab.com/basdat-b8/toysrent-b08/badges/master/pipeline.svg)](https://gitlab.com/basdat-b8/toysrent-b08/commits/master)

# Tugas Kelompok Basis Data (Kelas B Kelompok 8)  
  
Repository ini akan digunakan untuk pengembangan Tugas Kelompok mata kuliah Basis Data, Fakultas Ilmu Komputer, Universitas Indonesia. Pengerjaan proyek akhir ini mengambil studi kasus situs penyewaan permainan (Toys Rent)  
  
[Website Utama Toys Rent B08](https://toysrent-b8.herokuapp.com/ "Website Utama Toys Rent B8")    
[Backend API Toys Rent B08](https://toysrent-b8.herokuapp.com/api "Backend API Toys Rent B8")    

README Utama: Dokumen Ini  
README API Endpoint untuk Database: README-DB.md  
README Developer: README-DEV.md
  
> Website utama menggunakan hosting di Heroku dengan integrasi dengan PostgreSQL, sementara dokumentasi Backend API disusun oleh kelompok kami sendiri 

**Dirancang dengan </> dengan cinta oleh Kelompok 8 dari Kelas B**  
- Athallah Annafis (1706075022)  
- Falahdina Aprilia (1706043922)  
- Michael Christopher Manullang (1706039723)  
- Rd Pradipta Gitaya S (1706043361)