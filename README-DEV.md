# Perihal sebelum memulai mengerjakan

Pastikan sudah menggunakan versi pip yang terbaru. Kalau belum, lakukanlah update dengan menggunakan kode dibawah ini

`python -m pip install --upgrade pip`

Mulailah dengan men-setup environment dengan:

Menginstall Virtualenv

`pip install virtualenv`

Menjalankan kode ini untuk men-setup environment

`virtualenv env`

lalu masuk ke dalam environment yang sudah di setup

`env\scripts\activate`

Kemudian, install requirements dengan menjalankan kode dibawah ini

`pip install -r requirements.txt`

# Perihal pengerjaan

Setiap kali anda ingin menjalankan python untuk menjalankan file-file yang berhubungan dengan proyek ini, selalu masuk ke dalam environment yang sudah di setup
dengan menggunakan

`env\scripts\activate`

# Perihal template html

Untuk proyek ini, sudah ada template html yang berisi navbar. Navbar ini harus ditampilkan di setiap halaman agar tampilannya konsisten antar halaman.
Untuk melakukan itu, dalam semua html file yang anda buat, tolong tambahkan kode sesuai template ini

```
{% extends 'base.html' %}
{% load static %}
<html>
    <head>
            {style}
            (input kode head anda disini)
            {%block link %}
            (input kode yang bertag link disini)
        {% endblock %}
    </head>
    {% block content %}
    <body>
    (input kode body anda disini)
            {% if user.is_authenticated %}
            {% else %}
            {% endif %}
    </body>
</html>
{% endblock %}
```

## Penjelasan Kode

Extends akan men-set html yang anda buat menjadi anak dari base.html

Load static untuk men-load static files

Style untuk memberitahu program bahwa html anda akan menggunakan style dari base.html

Block link untuk memberitahukan kode untuk menggunakan kode yang bertag link untuk menampilkan halamannya

Block content untuk mendeklarasikan kepada program bahwa inilah hal-hal yang ingin ditampilkan bersama-sama dengan base.html

If else block digunakan untuk memaksa program untuk menampilkan sesuatu hal jika user telah login, dan hal lain jika pengguna belum melakukan login.
Gunakanlah dengan baik.

Jika anda ingin mengedit template html, anda bisa melakukannya dengan membuka folder templates di root folder proyek ini, dan kemudian meng-edit base.html yang ada di situ.

# Sebelum runserver di komputer anda 

Lakukanlah collectstatic

`python manage.py collectstatic`

dan ketik

`yes` 

untuk semua konfirmasi yang ada

setelah itu barulah anda boleh melakukan runserver dengan menjalankan kode ini

`python manage.py runserver`

# Hal-hal yang perlu diketahui

Proyek ini menggunakan bootstrap.

Error Urrlib3 dan dotenv dapat diselesaikan dengan masuk ke env dan install requirements.
