from .importer_modules import *
from .importer_model_serializers import BarangDikirimModel, BarangDikirimSerializer


@api_view(['GET'])
def barand_dikirim_list(request, format=None):
    if request.method == 'GET':
        items = BarangDikirimModel.db_objects.get_all_data_from_database()
        serializer = BarangDikirimSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def barang_dikirim_register(request, format=None):
    if request.method == 'POST':
        serializer = BarangDikirimSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def barang_dikirim_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]

    items = BarangDikirimModel.db_objects.get_data_from_database_by_id(id)
    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BarangDikirimSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def barang_dikirim_delete(request, no_resi, no_urut, format=None):
    no_resi = no_resi.lower()[:4].upper() + no_resi[4:]
    no_urut = no_urut.lower()[:6].upper() + no_urut[6:]

    items = BarangDikirimModel.db_objects.get_data_from_database_by_id(no_resi)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        for element in items:
            if element.no_urut == no_urut:
                items = element
                break

        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
