from .importer_modules import *
from .importer_model_serializers import BarangPesananModel, BarangPesananSerializer


@api_view(['GET'])
def barand_pesanan_list(request, format=None):
    if request.method == 'GET':
        items = BarangPesananModel.db_objects.get_all_data_from_database()
        serializer = BarangPesananSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def barang_pesanan_register(request, format=None):
    if request.method == 'POST':
        serializer = BarangPesananSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def barang_pesanan_detail(request, id, format=None):
    id = id.lower()[:3].upper() + id[3:]

    items = BarangPesananModel.db_objects.get_data_from_database_by_id(id)
    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BarangPesananSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def barang_pesanan_delete(request, id_pemesanan, no_urut, format=None):
    id_pemesanan = id_pemesanan.lower()[:3].upper() + id_pemesanan[3:]
    no_urut = no_urut.lower()[:6].upper() + no_urut[6:]

    items = BarangPesananModel.db_objects.get_data_from_database_by_id(id_pemesanan)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        for element in items:
            if element.no_urut == no_urut:
                items = element
                break

        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
