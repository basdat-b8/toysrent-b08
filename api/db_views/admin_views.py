from .importer_modules import *
from .importer_model_serializers import AdminModel, AdminSerializer


@api_view(['GET'])
def admin_list(request, format=None):
    if request.method == 'GET':
        admins = AdminModel.db_objects.get_all_data_from_database()
        serializer = AdminSerializer(admins, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def admin_register(request, format=None):
    if request.method == 'POST':
        serializer = AdminSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
def admin_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]
    
    admins = AdminModel.db_objects.get_data_from_database_by_id(id)
    
    if admins is None:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'DELETE':
        admins.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
