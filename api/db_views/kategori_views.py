from .importer_modules import *
from .importer_model_serializers import KategoriModel, KategoriSerializer


@api_view(['GET'])
def kategori_list(request, format=None):
    if request.method == 'GET':
        levels = KategoriModel.db_objects.get_all_data_from_database()
        serializer = KategoriSerializer(levels, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def kategori_register(request, format=None):
    if request.method == 'POST':
        serializer = KategoriSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def kategori_detail(request, id, format=None):
    id = id.lower()
    if id == "dolls-and-action-figures":
        id = "Dolls and Action Figures"
    else:
        id = (" ").join(id.split("-")).title()

    categories = KategoriModel.db_objects.get_data_from_database_by_id(id)

    if categories is None:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = KategoriSerializer(categories)
        return Response(serializer.data)
    if request.method == 'DELETE':
        categories.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
