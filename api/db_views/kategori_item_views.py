from .importer_modules import *
from .importer_model_serializers import KategoriItemModel, KategoriItemSerializer


@api_view(['GET'])
def kategori_item_list(request, format=None):
    if request.method == 'GET':
        items = KategoriItemModel.db_objects.get_all_data_from_database()
        serializer = KategoriItemSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def kategori_item_register(request, format=None):
    if request.method == 'POST':
        serializer = KategoriItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def kategori_item_detail(request, id, format=None):
    id = id.lower()
    if id == "boneka-winnie-the-pooh":
        id = "Boneka Winnie the Pooh"
    elif id == "action-figure-sao":
        id = "Action Figure SAO"
    else:
        id = (" ").join(id.split("-")).title()

    items = KategoriItemModel.db_objects.get_data_from_database_by_id(id)
    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = KategoriItemSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def kategori_item_delete(request, nama_item, nama_kategori, format=None):
    if nama_item.lower() == "boneka-winnie-the-pooh":
        nama_item = "Boneka Winnie the Pooh"
    elif nama_item.lower() == "action-figure-sao":
        nama_item = "Action Figure SAO"
    else:
        nama_item = (" ").join(nama_item.split("-")).title()

    if nama_kategori.lower() == "dolls-and-action-figures":
        nama_kategori = "Dolls and Action Figures"
    else:
        nama_kategori = (" ").join(nama_kategori.split("-")).title()

    items = KategoriItemModel.db_objects.get_data_from_database_by_id(nama_item)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        for element in items:
            if element.nama_kategori == nama_kategori:
                items = element
                break

        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
