from .importer_modules import *
from .importer_model_serializers import InfoBarangLevelModel, InfoBarangLevelSerializer


@api_view(['GET'])
def info_barang_level_list(request, format=None):
    if request.method == 'GET':
        items = InfoBarangLevelModel.db_objects.get_all_data_from_database()
        serializer = InfoBarangLevelSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def info_barang_level_register(request, format=None):
    if request.method == 'POST':
        serializer = InfoBarangLevelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def info_barang_level_detail(request, id, format=None):
    id = id.lower()[:6].upper() + id[6:]

    items = InfoBarangLevelModel.db_objects.get_data_from_database_by_id(id)
    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = InfoBarangLevelSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def info_barang_level_delete(request, id_barang, nama_level, format=None):
    id_barang = id_barang.lower()[:6].upper() + id_barang[6:]
    nama_level = nama_level.lower().capitalize()

    items = InfoBarangLevelModel.db_objects.get_data_from_database_by_id(id_barang)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        for element in items:
            if element.nama_level == nama_level:
                items = element
                break

        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
