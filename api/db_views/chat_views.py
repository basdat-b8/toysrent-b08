from .importer_modules import *
from .importer_model_serializers import ChatModel, ChatSerializer


@api_view(['GET'])
def chat_list(request, format=None):
    if request.method == 'GET':
        users = ChatModel.db_objects.get_all_data_from_database()
        serializer = ChatSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def chat_register(request, format=None):
    if request.method == 'POST':
        serializer = ChatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def chat_detail(request, id, format=None):
    id = id.lower()[:3].upper() + id[3:]

    chats = ChatModel.db_objects.get_data_from_database_by_id(id)

    if chats is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ChatSerializer(chats)
        return Response(serializer.data)

    if request.method == 'DELETE':
        chats.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
