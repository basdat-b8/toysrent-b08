from .importer_modules import *
from .importer_model_serializers import AlamatModel, AlamatSerializer


@api_view(['GET'])
def alamat_list(request, format=None):
    if request.method == 'GET':
        users = AlamatModel.db_objects.get_all_data_from_database()
        serializer = AlamatSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def alamat_register(request, format=None):
    if request.method == 'POST':
        serializer = AlamatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def alamat_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]

    address = AlamatModel.db_objects.get_data_from_database_by_id(id)

    if address is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AlamatSerializer(address)
        return Response(serializer.data)

    if request.method == 'DELETE':
        address.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
