from .importer_modules import *
from .importer_model_serializers import LevelKeanggotaanModel, LevelKeanggotaanSerializer


@api_view(['GET'])
def level_keanggotaan_list(request, format=None):
    if request.method == 'GET':
        levels = LevelKeanggotaanModel.db_objects.get_all_data_from_database()
        serializer = LevelKeanggotaanSerializer(levels, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def level_keanggotaan_register(request, format=None):
    if request.method == 'POST':
        serializer = LevelKeanggotaanSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def level_keanggotaan_detail(request, id, format=None):
    id = id.lower().capitalize()

    levels = LevelKeanggotaanModel.db_objects.get_data_from_database_by_id(id)

    if levels is None:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = LevelKeanggotaanSerializer(levels)
        return Response(serializer.data)
    if request.method == 'DELETE':
        levels.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
