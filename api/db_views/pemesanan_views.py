from .importer_modules import *
from .importer_model_serializers import PemesananModel, PemesananSerializer


@api_view(['GET'])
def pemesanan_list(request, format=None):
    if request.method == 'GET':
        items = PemesananModel.db_objects.get_all_data_from_database()
        serializer = PemesananSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def pemesanan_register(request, format=None):
    if request.method == 'POST':
        serializer = PemesananSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def pemesanan_detail(request, id, format=None):
    id = id.lower()[:3].upper() + id[3:]

    items = PemesananModel.db_objects.get_data_from_database_by_id(id)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PemesananSerializer(items)
        return Response(serializer.data)

    if request.method == 'DELETE':
        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
