from .importer_modules import *
from .importer_model_serializers import BarangModel, BarangSerializer


@api_view(['GET'])
def barang_list(request, format=None):
    if request.method == 'GET':
        items = BarangModel.db_objects.get_all_data_from_database()
        serializer = BarangSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def barang_register(request, format=None):
    if request.method == 'POST':
        serializer = BarangSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def barang_detail(request, id, format=None):
    id = id.lower()[:6].upper() + id[6:]

    items = BarangModel.db_objects.get_data_from_database_by_id(id)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BarangSerializer(items)
        return Response(serializer.data)

    if request.method == 'DELETE':
        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
