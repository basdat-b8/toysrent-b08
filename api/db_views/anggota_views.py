from .importer_modules import *
from .importer_model_serializers import AnggotaModel, AnggotaSerializer


@api_view(['GET'])
def anggota_list(request, format=None):
    if request.method == 'GET':
        members = AnggotaModel.db_objects.get_all_data_from_database()
        serializer = AnggotaSerializer(members, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def anggota_register(request, format=None):
    if request.method == 'POST':
        serializer = AnggotaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def anggota_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]
    
    members = AnggotaModel.db_objects.get_data_from_database_by_id(id)

    if members is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AnggotaSerializer(members)
        return Response(serializer.data)

    if request.method == 'DELETE':
        members.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
