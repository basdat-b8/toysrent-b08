from .importer_modules import *
from .importer_model_serializers import PenggunaModel, PenggunaSerializer


@api_view(['GET'])
def pengguna_list(request, format=None):
    if request.method == 'GET':
        users = PenggunaModel.db_objects.get_all_data_from_database()
        serializer = PenggunaSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def pengguna_register(request, format=None):
    if request.method == 'POST':
        serializer = PenggunaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def pengguna_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]

    users = PenggunaModel.db_objects.get_data_from_database_by_id(id)

    if users is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PenggunaSerializer(users)
        return Response(serializer.data)

    if request.method == 'DELETE':
        users.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)
