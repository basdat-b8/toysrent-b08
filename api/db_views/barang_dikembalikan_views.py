from .importer_modules import *
from .importer_model_serializers import BarangDikembalikanModel, BarangDikembalikanSerializer


@api_view(['GET'])
def barang_dikembalikan_list(request, format=None):
    if request.method == 'GET':
        items = BarangDikembalikanModel.db_objects.get_all_data_from_database()
        serializer = BarangDikembalikanSerializer(items, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def barang_dikembalikan_register(request, format=None):
    if request.method == 'POST':
        serializer = BarangDikembalikanSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'DELETE'])
def barang_dikembalikan_detail(request, id, format=None):
    id = id.lower()[:4].upper() + id[4:]

    items = BarangDikembalikanModel.db_objects.get_data_from_database_by_id(id)

    if items is None:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BarangDikembalikanSerializer(items)
        return Response(serializer.data)

    if request.method == 'DELETE':
        items.init_delete_data_from_database()
        return HttpResponse(status=status.HTTP_200_OK)