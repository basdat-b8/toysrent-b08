from django.urls import path
from .views import APIPage

from .db_views.admin_views import admin_list, admin_register, admin_detail
from .db_views.item_views import item_list, item_register, item_detail
from .db_views.level_keanggotaan_views import level_keanggotaan_list, level_keanggotaan_register,level_keanggotaan_detail
from .db_views.pengguna_views import pengguna_list, pengguna_register, pengguna_detail
from .db_views.anggota_views import anggota_list, anggota_register, anggota_detail
from .db_views.alamat_views import alamat_list, alamat_register, alamat_detail
from .db_views.barang_views import barang_list, barang_register, barang_detail
from .db_views.barang_dikembalikan_views import barang_dikembalikan_list, barang_dikembalikan_register, barang_dikembalikan_detail
from .db_views.barang_dikirim_views import barand_dikirim_list, barang_dikirim_register, barang_dikirim_detail, barang_dikirim_delete
from .db_views.barang_pesanan_views import barand_pesanan_list, barang_pesanan_register, barang_pesanan_detail, barang_pesanan_delete
from .db_views.chat_views import chat_list, chat_register, chat_detail
from .db_views.info_barang_level_views import info_barang_level_list, info_barang_level_register, info_barang_level_detail, info_barang_level_delete
from .db_views.kategori_views import kategori_list, kategori_register, kategori_detail
from .db_views.kategori_item_views import kategori_item_list, kategori_item_register, kategori_item_detail, kategori_item_delete
from .db_views.pemesanan_views import pemesanan_list, pemesanan_register, pemesanan_detail
from .db_views.pengembalian_views import pengembalian_list, pengembalian_register, pengembalian_detail

from .db_views.pengiriman_views import pengiriman_list, pengiriman_register, pengiriman_detail

app_name = "toysrent-api"

urlpatterns = [
    # API Home Page
    path('', APIPage.as_view(), name="api_page"),

    # Users (Pengguna + Admin)
    path('users/register/', pengguna_register, name="pengguna_register"),
    path('users/all/', pengguna_list, name="pengguna_list"),
    path('users/<str:id>/', pengguna_detail, name="pengguna_detail"),
    
    # Admin
    path('admins/register/', admin_register, name="admin_register"),
    path('admins/all/', admin_list, name="admin_list"),
    path('admins/<str:id>/', admin_detail, name="admin_detail"),
    
    # Members (Pengguna saja)
    path('members/register/', anggota_register, name="anggota_register"),
    path('members/all/', anggota_list, name="anggota_list"),
    path('members/<str:id>/', anggota_detail, name="anggota_detail"),
    
    # Levels (Daftar Level Keanggotaan)
    path('levels/register/', level_keanggotaan_register, name="level_keanggotaan_register"),
    path('levels/all/', level_keanggotaan_list, name="level_keanggotaan_list"),
    path('levels/<str:id>/', level_keanggotaan_detail, name="level_keanggotaan_detail"),
    
    # Items
    path('items/register/', item_register, name="item_register"),
    path('items/all/', item_list, name="item_list"),
    path('items/<str:id>/', item_detail, name="item_detail"),

    # Alamat
    path('address/register/', alamat_register, name="alamat_register"),
    path('address/all/', alamat_list, name="alamat_list"),
    path('address/<str:id>/', alamat_detail, name="alamat_detail"),

    # Barang
    path('products/register/', barang_register, name="barang_register"),
    path('products/all/', barang_list, name="barang_list"),
    path('products/<str:id>/', barang_detail, name="barang_detail"),

    # Barang Dikembalikan
    path('products/return/register/', barang_dikembalikan_register, name="barang_dikembalikan_register"),
    path('products/return/all/', barang_dikembalikan_list, name="barang_dikembalikan_list"),
    path('products/return/<str:id>/', barang_dikembalikan_detail, name="barang_dikembalikan_detail"),

    # Barang Dikirim
    path('products/delivery/register/', barang_dikirim_register, name="barang_dikirim_register"),
    path('products/delivery/all/', barand_dikirim_list, name="barang_dikirim_list"),
    path('products/delivery/<str:id>/', barang_dikirim_detail, name="barang_dikirim_detail"),
    path('products/delivery/<str:no_resi>/<str:no_urut>/', barang_dikirim_delete, name="barang_dikirim_delete"),

    # Barang Pesanan
    path('products/order/register/', barang_pesanan_register, name="barang_pesanan_register"),
    path('products/order/all/', barand_pesanan_list, name="barang_pesanan_list"),
    path('products/order/<str:id>/', barang_pesanan_detail, name="barang_pesanan_detail"),
    path('products/order/<str:id_pemesanan>/<str:no_urut>/', barang_pesanan_delete, name="barang_pesanan_delete"),

    # Chats
    path('chats/register/', chat_register, name="chat_register"),
    path('chats/all/', chat_list, name="chat_list"),
    path('chats/<str:id>/', chat_detail, name="chat_detail"),

    # Info Barang Level
    path('products/level/register/', info_barang_level_register, name="info_barang_level_register"),
    path('products/level/all/', info_barang_level_list, name="info_barang_level_list"),
    path('products/level/<str:id>/', info_barang_level_detail, name="info_barang_level_detail"),
    path('products/level/<str:id_barang>/<str:nama_level>/', info_barang_level_delete, name="info_barang_level_delete"),

    # Kategori
    path('categories/register/', kategori_register, name="kategori_register"),
    path('categories/all/', kategori_list, name="kategori_list"),
    path('categories/<str:id>/', kategori_detail, name="kategori_detail"),

    # Kategori Item
    path('items/category/register/', kategori_item_register, name="kategori_item_register"),
    path('items/category/all/', kategori_item_list, name="kategori_item_list"),
    path('items/category/<str:id>/', kategori_item_detail, name="kategori_item_detail"),
    path('items/category/<str:nama_item>/<str:nama_kategori>/', kategori_item_delete, name="kategori_item_delete"),

    # Pemesanan
    path('orders/register/', pemesanan_register, name="pemesanan_register"),
    path('orders/all/', pemesanan_list, name="pemesanan_list"),
    path('orders/<str:id>/', pemesanan_detail, name="pemesanan_detail"),

    # Pengembalian
    path('returns/register/', pengembalian_register, name="pengembalian_register"),
    path('returns/all/', pengembalian_list, name="pengembalian_list"),
    path('returns/<str:id>/', pengembalian_detail, name="pengembalian_detail"),

    # Pengiriman
    path('delivery/register/', pengiriman_register, name="pengiriman_register"),
    path('delivery/all/', pengiriman_list, name="pengiriman_list"),
    path('delivery/<str:id>/', pengiriman_detail, name="pengiriman_detail"),

]
