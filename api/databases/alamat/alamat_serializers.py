from rest_framework import serializers
from .alamat_models import AlamatModel


class AlamatSerializer(serializers.Serializer):
    no_ktp_anggota = serializers.CharField(max_length=20)
    nama = serializers.CharField(max_length=255)
    jalan = serializers.CharField(required=True, allow_blank=False, max_length=255)
    nomor = serializers.IntegerField(required=True)
    kota = serializers.CharField(required=True, allow_blank=False, max_length=255)
    kodepos = serializers.CharField(required=False, allow_blank=True, max_length=10)

    def create(self, validated_data):
        db_object = AlamatModel()
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota")
        db_object.nama = validated_data.get("nama")
        db_object.jalan = validated_data.get("jalan")
        db_object.nomor = validated_data.get("nomor")
        db_object.kota = validated_data.get("kota")
        db_object.kodepos = validated_data.get("kodepos")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota", db_object.no_ktp_anggota)
        db_object.nama = validated_data.get("nama", db_object.nama)
        db_object.jalan = validated_data.get("jalan", db_object.jalan)
        db_object.nomor = validated_data.get("nomor", db_object.nomor)
        db_object.kota = validated_data.get("kota", db_object.kota)
        db_object.kodepos = validated_data.get("kodepos", db_object.kodepos)
        db_object.init_save_data_to_database()
        return db_object
