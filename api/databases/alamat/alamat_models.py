from .alamat_db import AlamatDB
from ..ModelManager import ModelManager


class AlamatModel(ModelManager):
    db_objects = AlamatDB()

    def __init__(self):
        self.no_ktp_anggota = None
        self.nama = None
        self.jalan = None
        self.nomor = None
        self.kota = None
        self.kodepos = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
