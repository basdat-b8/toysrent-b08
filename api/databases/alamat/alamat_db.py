from ..DBManager import DBManager


class AlamatDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO alamat
                   (no_ktp_anggota,nama,jalan,nomor,kota,kodepos)
                   VALUES ('{0}','{1}','{2}','{3}','{4}','{5}');""".format(
            db_models.no_ktp_anggota,
            db_models.nama,
            db_models.jalan,
            db_models.nomor,
            db_models.kota,
            db_models.kodepos
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM alamat;"""
        cursor = super().get_all_data_from_database(database_query)
        from .alamat_models import AlamatModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AlamatModel()
            db_object.no_ktp_anggota = db_row[0]
            db_object.nama = db_row[1]
            db_object.jalan = db_row[2]
            db_object.nomor = db_row[3]
            db_object.kota = db_row[4]
            db_object.kodepos = db_row[5]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM alamat
                            WHERE no_ktp_anggota='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .alamat_models import AlamatModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AlamatModel()
            db_object.no_ktp_anggota = db_row[0]
            db_object.nama = db_row[1]
            db_object.jalan = db_row[2]
            db_object.nomor = db_row[3]
            db_object.kota = db_row[4]
            db_object.kodepos = db_row[5]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE alamat
                            SET no_ktp_anggota='{0}',
                            nama='{1}',
                            jalan='{2}',
                            nomor='{3}',
                            kota='{4}',
                            kodepos='{5}'
                            WHERE no_ktp_anggota='{0}';""".format(
            db_models.no_ktp_anggota,
            db_models.nama,
            db_models.jalan,
            db_models.nomor,
            db_models.kota,
            db_models.kodepos
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM alamat
                            WHERE no_ktp_anggota='{0}';""".format(db_models.no_ktp_anggota)
        super().delete_data_from_database(database_query)
        return
