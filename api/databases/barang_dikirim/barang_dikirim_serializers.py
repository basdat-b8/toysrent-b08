from rest_framework import serializers
from .barang_dikirim_models import BarangDikirimModel


class BarangDikirimSerializer(serializers.Serializer):
    no_resi = serializers.CharField(max_length=10)
    no_urut = serializers.CharField(max_length=10)
    id_barang = serializers.CharField(max_length=10)
    tanggal_review = serializers.DateField(required=True)
    review = serializers.CharField(required=False, allow_blank=True, max_length=255)

    def create(self, validated_data):
        db_object = BarangDikirimModel()
        db_object.no_resi = validated_data.get("no_resi")
        db_object.no_urut = validated_data.get("no_urut")
        db_object.id_barang = validated_data.get("id_barang")
        db_object.tanggal_review = validated_data.get("tanggal_review")
        db_object.review = validated_data.get("review")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_resi = validated_data.get("no_resi", db_object.no_resi)
        db_object.no_urut = validated_data.get("no_urut", db_object.no_urut)
        db_object.id_barang = validated_data.get("id_barang", db_object.id_barang)
        db_object.tanggal_review = validated_data.get("tanggal_review", db_object.tanggal_review)
        db_object.review = validated_data.get("review", db_object.review)
        db_object.init_save_data_to_database()
        return db_object
