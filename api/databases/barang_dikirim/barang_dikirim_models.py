from .barang_dikirim_db import BarangDikirimDB
from ..ModelManager import ModelManager


class BarangDikirimModel(ModelManager):
    db_objects = BarangDikirimDB()

    def __init__(self):
        self.no_resi = None
        self.no_urut = None
        self.id_barang = None
        self.tanggal_review = None
        self.review = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
