from ..DBManager import DBManager


class BarangDikirimDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO barang_dikirim
                   (no_resi,no_urut,id_barang,tanggal_review,review)
                   VALUES ('{0}','{1}','{2}','{3}','{4}');""".format(
            db_models.no_resi,
            db_models.no_urut,
            db_models.id_barang,
            db_models.tanggal_review,
            db_models.review
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM barang_dikirim;"""
        cursor = super().get_all_data_from_database(database_query)
        from .barang_dikirim_models import BarangDikirimModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangDikirimModel()
            db_object.no_resi = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            db_object.tanggal_review = db_row[3]
            db_object.review = db_row[4]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM barang_dikirim
                            WHERE no_resi='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .barang_dikirim_models import BarangDikirimModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangDikirimModel()
            db_object.no_resi = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            db_object.tanggal_review = db_row[3]
            db_object.review = db_row[4]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects

    def update_data_to_database(self, db_models):
        database_query = """UPDATE barang_dikirim
                            SET no_resi='{0}',
                            no_urut='{1}',
                            id_barang='{2}',
                            tanggal_review='{3}',
                            review='{4}'
                            WHERE no_resi='{0}'
                            AND no_urut='{1}';""".format(
            db_models.no_resi,
            db_models.no_urut,
            db_models.id_barang,
            db_models.tanggal_review,
            db_models.review
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM barang_dikirim
                            WHERE no_resi='{0}'
                            AND no_urut='{1}';""".format(db_models.no_resi, db_models.no_urut)
        super().delete_data_from_database(database_query)
        return
