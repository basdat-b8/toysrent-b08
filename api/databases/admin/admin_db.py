from ..DBManager import DBManager


class AdminDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO admin
                   (no_ktp)
                   VALUES ('{0}');""".format(db_models.no_ktp)
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM admin;"""
        cursor = super().get_all_data_from_database(database_query)

        from .admin_models import AdminModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AdminModel()
            db_object.no_ktp = db_row[0]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM admin
                            WHERE no_ktp='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .admin_models import AdminModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AdminModel()
            db_object.no_ktp = db_row[0]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE admin
                            SET no_ktp='{0}';""".format(db_models.no_ktp)
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM admin
                            WHERE no_ktp='{0}';""".format(db_models.no_ktp)
        super().delete_data_from_database(database_query)
        return
