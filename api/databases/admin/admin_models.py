from .admin_db import AdminDB
from ..ModelManager import ModelManager


class AdminModel(ModelManager):
    db_objects = AdminDB()

    def __init__(self):
        self.no_ktp = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
