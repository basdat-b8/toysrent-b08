from rest_framework import serializers
from .admin_models import AdminModel


class AdminSerializer(serializers.Serializer):
    no_ktp = serializers.CharField(max_length=20)

    def create(self, validated_data):
        db_object = AdminModel()
        db_object.no_ktp = validated_data.get("no_ktp")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_ktp = validated_data.get("no_ktp", db_object.no_ktp)
        db_object.init_save_data_to_database()
        return db_object
