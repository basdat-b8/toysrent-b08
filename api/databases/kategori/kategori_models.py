from .kategori_db import KategoriDB
from ..ModelManager import ModelManager


class KategoriModel(ModelManager):
    db_objects = KategoriDB()

    def __init__(self):
        self.nama = None
        self.level = None
        self.sub_dari = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
