from rest_framework import serializers
from .kategori_models import KategoriModel


class KategoriSerializer(serializers.Serializer):
    nama = serializers.CharField(max_length=255)
    level = serializers.IntegerField(required=True)
    sub_dari = serializers.CharField(max_length=255)

    def create(self, validated_data):
        print("-------------------------------------")
        db_object = KategoriModel()
        db_object.nama = validated_data.get("nama")
        db_object.level = validated_data.get("level")
        db_object.sub_dari = validated_data.get("sub_dari")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.nama = validated_data.get("nama", db_object.nama)
        db_object.level = validated_data.get("level", db_object.level)
        db_object.sub_dari = validated_data.get("sub_dari", db_object.sub_dari)
        db_object.init_save_data_to_database()
        return db_object
