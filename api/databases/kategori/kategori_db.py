from ..DBManager import DBManager


class KategoriDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO kategori
                   (nama,level,sub_dari)
                   VALUES ('{0}','{1}','{2}');""".format(
            db_models.nama,
            db_models.level,
            db_models.sub_dari)
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM kategori;"""
        cursor = super().get_all_data_from_database(database_query)
        from .kategori_models import KategoriModel
        list_of_db_objects = []
        for db_row in cursor.fetchall():
            db_object = KategoriModel()
            db_object.nama = db_row[0]
            db_object.level = db_row[1]
            db_object.sub_dari = db_row[2]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT nama,
                            level,
                            sub_dari
                            FROM kategori
                            WHERE nama='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .kategori_models import KategoriModel
        list_of_db_objects = []
        for db_row in cursor.fetchall():
            db_object = KategoriModel()
            db_object.nama = db_row[0]
            db_object.level = db_row[1]
            db_object.sub_dari = db_row[2]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE kategori
                            SET nama='{0}',
                            level='{1}',
                            sub_dari='{2}'
                            WHERE nama='{0}';""".format(
            db_models.nama,
            db_models.level,
            db_models.sub_dari)
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM kategori
                            WHERE nama='{0}';""".format(db_models.nama)
        super().delete_data_from_database(database_query)
        return
