from .barang_dikembalikan_db import BarangDikembalikanDB
from ..ModelManager import ModelManager


class BarangDikembalikanModel(ModelManager):
    db_objects = BarangDikembalikanDB()

    def __init__(self):
        self.no_resi = None
        self.no_urut = None
        self.id_barang = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
