from ..DBManager import DBManager


class BarangDikembalikanDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO barang_dikembalikan
                   (no_resi, no_urut, id_barang)
                   VALUES ('{0}','{1}','{2}');""".format(
            db_models.no_resi,
            db_models.no_urut,
            db_models.id_barang
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM barang_dikembalikan;"""
        cursor = super().get_all_data_from_database(database_query)
        from .barang_dikembalikan_models import BarangDikembalikanModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangDikembalikanModel()
            db_object.no_resi = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM barang_dikembalikan
                            WHERE no_resi='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .barang_dikembalikan_models import BarangDikembalikanModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangDikembalikanModel()
            db_object.no_resi = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE barang_dikembalikan
                            SET no_resi='{0}',
                            no_urut='{1}',
                            id_barang='{2}'
                            WHERE no_resi='{0}';""".format(
            db_models.no_resi,
            db_models.no_urut,
            db_models.id_barang
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM barang_dikembalikan
                            WHERE no_resi='{0}';""".format(db_models.no_resi)
        super().delete_data_from_database(database_query)
        return
