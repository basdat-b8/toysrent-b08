from rest_framework import serializers
from .barang_dikembalikan_models import BarangDikembalikanModel


class BarangDikembalikanSerializer(serializers.Serializer):
    no_resi = serializers.CharField(max_length=10)
    no_urut = serializers.CharField(max_length=10)
    id_barang = serializers.CharField(max_length=10)

    def create(self, validated_data):
        db_object = BarangDikembalikanModel()
        db_object.no_resi = validated_data.get("no_resi")
        db_object.no_urut = validated_data.get("no_urut")
        db_object.id_barang = validated_data.get("id_barang")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_resi = validated_data.get("no_resi", db_object.no_resi)
        db_object.no_urut = validated_data.get("no_urut", db_object.no_urut)
        db_object.id_barang = validated_data.get("id_barang", db_object.id_barang)
        db_object.init_save_data_to_database()
        return db_object
