from django.db import connection


class DBManager():

    def set_database_path(self):
        print("Executing SQL command to SET SEARCH PATH...")
        cursor = connection.cursor()
        database_query = "SET SEARCH_PATH TO TOYS_RENT;"
        cursor.execute(database_query)
        print("SET SEARCH PATH success...")
        return

    def insert_data_to_database(self, database_query):
        print("Executing SQL command to INSERT data...")
        DBManager.set_database_path(self)
        cursor = connection.cursor()
        cursor.execute(database_query)
        print("Execution to INSERT data success...")
        return

    def get_all_data_from_database(self, database_query):
        print("Executing SQL command to GET ALL data...")
        DBManager.set_database_path(self)
        cursor = connection.cursor()
        cursor.execute(database_query)
        print("Execution to GET ALL data success...")
        return cursor

    def get_data_from_database_by_id(self, database_query):
        print("Executing SQL command to GET DATA BY ID...")
        DBManager.set_database_path(self)
        cursor = connection.cursor()
        cursor.execute(database_query)
        print("Execution to GET DATA BY ID success...")
        return cursor

    def update_data_to_database(self, database_query):
        print("Executing SQL command to UPDATE data...")
        DBManager.set_database_path(self)
        cursor = connection.cursor()
        cursor.execute(database_query)
        print("Execution to UPDATE data success...")
        return

    def delete_data_from_database(self, database_query):
        print("Executing SQL command to DELETE DATA...")
        DBManager.set_database_path(self)
        cursor = connection.cursor()
        cursor.execute(database_query)
        print("Execution to DELETE DATA success...")
        return
