from .pemesanan_db import PemesananDB
from ..ModelManager import ModelManager


class PemesananModel(ModelManager):
    db_objects = PemesananDB()

    def __init__(self):
        self.id_pemesanan = None
        self.datetime_pesanan = None
        self.kuantitas_barang = None
        self.harga_sewa = None
        self.ongkos = None
        self.no_ktp_pemesan = None
        self.status = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
