from ..DBManager import DBManager


class PemesananDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO pemesanan
                   (id_pemesanan,datetime_pesanan,kuantitas_barang,harga_sewa,ongkos,no_ktp_pemesan,status)
                   VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}');""".format(
            db_models.id_pemesanan,
            db_models.datetime_pesanan,
            db_models.kuantitas_barang,
            db_models.harga_sewa,
            db_models.ongkos,
            db_models.no_ktp_pemesan,
            db_models.status
        )
        print(database_query)
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM pemesanan;"""
        cursor = super().get_all_data_from_database(database_query)
        from .pemesanan_models import PemesananModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PemesananModel()
            db_object.id_pemesanan = db_row[0]
            db_object.datetime_pesanan = db_row[1]
            db_object.kuantitas_barang = db_row[2]
            db_object.harga_sewa = db_row[3]
            db_object.ongkos = db_row[4]
            db_object.no_ktp_pemesan = db_row[5]
            db_object.status = db_row[6]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM pemesanan
                            WHERE id_pemesanan='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .pemesanan_models import PemesananModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PemesananModel()
            db_object.id_pemesanan = db_row[0]
            db_object.datetime_pesanan = db_row[1]
            db_object.kuantitas_barang = db_row[2]
            db_object.harga_sewa = db_row[3]
            db_object.ongkos = db_row[4]
            db_object.no_ktp_pemesan = db_row[5]
            db_object.status = db_row[6]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE pemesanan
                            SET id_pemesanan='{0}',
                            datetime_pesanan='{1}',
                            kuantitas_barang='{2}',
                            harga_sewa='{3}',
                            ongkos='{4}',
                            no_ktp_pemesan='{5}',
                            status='{6}'
                            WHERE id_pemesanan='{0}';""".format(
            db_models.id_pemesanan,
            db_models.datetime_pesanan,
            db_models.kuantitas_barang,
            db_models.harga_sewa,
            db_models.ongkos,
            db_models.no_ktp_pemesan,
            db_models.status
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM pemesanan
                            WHERE id_pemesanan='{0}';""".format(db_models.id_pemesanan)
        super().delete_data_from_database(database_query)
        return
