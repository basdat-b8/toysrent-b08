from rest_framework import serializers
from .pemesanan_models import PemesananModel


class PemesananSerializer(serializers.Serializer):
    id_pemesanan = serializers.CharField(max_length=10)
    datetime_pesanan = serializers.DateTimeField(required=True)
    kuantitas_barang = serializers.IntegerField(required=True)
    harga_sewa = serializers.IntegerField()
    ongkos = serializers.IntegerField()
    no_ktp_pemesan = serializers.CharField(required=True, allow_blank=False, max_length=20)
    status = serializers.CharField(max_length=50)

    def create(self, validated_data):
        db_object = PemesananModel()
        db_object.id_pemesanan = validated_data.get("id_pemesanan")
        db_object.datetime_pesanan = validated_data.get("datetime_pesanan")
        db_object.kuantitas_barang = validated_data.get("kuantitas_barang")
        db_object.harga_sewa = validated_data.get("harga_sewa")
        db_object.ongkos = validated_data.get("ongkos")
        db_object.no_ktp_pemesan = validated_data.get("no_ktp_pemesan")
        db_object.status = validated_data.get("status")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.id_pemesanan = validated_data.get("id_pemesanan", db_object.id_pemesanan)
        db_object.datetime_pesanan = validated_data.get("datetime_pesanan", db_object.datetime_pesanan)
        db_object.kuantitas_barang = validated_data.get("kuantitas_barang", db_object.kuantitas_barang)
        db_object.harga_sewa = validated_data.get("harga_sewa", db_object.harga_sewa)
        db_object.ongkos = validated_data.get("ongkos", db_object.ongkos)
        db_object.no_ktp_pemesan = validated_data.get("no_ktp_pemesan", db_object.no_ktp_pemesan)
        db_object.status = validated_data.get("status", db_object.status)
        db_object.init_save_data_to_database()
        return db_object
