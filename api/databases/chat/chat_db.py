from ..DBManager import DBManager


class ChatDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO chat
                   (id,pesan,date_time,no_ktp_anggota,no_ktp_admin)
                   VALUES ('{0}','{1}','{2}','{3}','{4}');""".format(
            db_models.id,
            db_models.pesan,
            db_models.date_time,
            db_models.no_ktp_anggota,
            db_models.no_ktp_admin
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM chat;"""
        cursor = super().get_all_data_from_database(database_query)
        from .chat_models import ChatModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = ChatModel()
            db_object.id = db_row[0]
            db_object.pesan = db_row[1]
            db_object.date_time = db_row[2]
            db_object.no_ktp_anggota = db_row[3]
            db_object.no_ktp_admin = db_row[4]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM chat
                            WHERE id='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .chat_models import ChatModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = ChatModel()
            db_object.id = db_row[0]
            db_object.pesan = db_row[1]
            db_object.date_time = db_row[2]
            db_object.no_ktp_anggota = db_row[3]
            db_object.no_ktp_admin = db_row[4]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE chat
                            SET id='{0}',
                            pesan='{1}',
                            date_time='{2}',
                            no_ktp_anggota='{3}',
                            no_ktp_admin='{4}'
                            WHERE id='{0}';""".format(
            db_models.id,
            db_models.pesan,
            db_models.date_time,
            db_models.no_ktp_anggota,
            db_models.no_ktp_admin
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM chat
                            WHERE id='{0}';""".format(db_models.id)
        super().delete_data_from_database(database_query)
        return
