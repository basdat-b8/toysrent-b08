from .chat_db import ChatDB
from ..ModelManager import ModelManager


class ChatModel(ModelManager):
    db_objects = ChatDB()

    def __init__(self):
        self.id = None
        self.pesan = None
        self.date_time = None
        self.no_ktp_anggota = None
        self.no_ktp_admin = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
