from rest_framework import serializers
from .chat_models import ChatModel


class ChatSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=15)
    pesan = serializers.CharField(required=True, allow_blank=False, max_length=1000)
    date_time = serializers.DateTimeField(required=True)
    no_ktp_anggota = serializers.CharField(required=True,allow_blank=False, max_length=20)
    no_ktp_admin = serializers.CharField(required=True, allow_blank=False, max_length=20)

    def create(self, validated_data):
        db_object = ChatModel()
        db_object.id = validated_data.get("id")
        db_object.pesan = validated_data.get("pesan")
        db_object.date_time = validated_data.get("date_time")
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota")
        db_object.no_ktp_admin = validated_data.get("no_ktp_admin")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.id = validated_data.get("id", db_object.id)
        db_object.pesan = validated_data.get("pesan", db_object.pesan)
        db_object.date_time = validated_data.get("date_time", db_object.date_time)
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota", db_object.no_ktp_anggota)
        db_object.no_ktp_admin = validated_data.get("admin", db_object.admin)
        db_object.init_save_data_to_database()
        return db_object
