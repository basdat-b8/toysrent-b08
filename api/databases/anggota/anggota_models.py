from .anggota_db import AnggotaDB
from ..ModelManager import ModelManager


class AnggotaModel(ModelManager):
    db_objects = AnggotaDB()

    def __init__(self):
        self.no_ktp = None
        self.poin = None
        self.level = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
