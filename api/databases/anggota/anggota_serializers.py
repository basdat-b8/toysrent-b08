from rest_framework import serializers
from .anggota_models import AnggotaModel


class AnggotaSerializer(serializers.Serializer):
    no_ktp = serializers.CharField(max_length=20)
    poin = serializers.IntegerField(required=True);
    level = serializers.CharField(max_length=255)

    def create(self, validated_data):
        db_object = AnggotaModel()
        db_object.no_ktp = validated_data.get("no_ktp")
        db_object.poin = validated_data.get("poin")
        db_object.level = validated_data.get("level")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_ktp = validated_data.get("no_ktp", db_object.no_ktp)
        db_object.poin = validated_data.get("poin", db_object.poin)
        db_object.level = validated_data.get("level", db_object.level)
        db_object.init_save_data_to_database()
        return db_object
