from ..DBManager import DBManager


class AnggotaDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO anggota
                   (no_ktp, poin, level)
                   VALUES ('{0}','{1}','{2}');""".format(
            db_models.no_ktp,
            db_models.poin,
            db_models.level
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM anggota;"""
        cursor = super().get_all_data_from_database(database_query)
        from .anggota_models import AnggotaModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AnggotaModel()
            db_object.no_ktp = db_row[0]
            db_object.poin = db_row[1]
            db_object.level = db_row[2]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM anggota
                            WHERE no_ktp='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .anggota_models import AnggotaModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = AnggotaModel()
            db_object.no_ktp = db_row[0]
            db_object.poin = db_row[1]
            db_object.level = db_row[2]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE anggota
                            SET no_ktp='{0}',
                            poin='{1}',
                            level='{2}'
                            WHERE no_ktp='{0}';""".format(
            db_models.no_ktp,
            db_models.poin,
            db_models.level
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM anggota
                            WHERE no_ktp='{0}';""".format(db_models.no_ktp)
        super().delete_data_from_database(database_query)
        return
