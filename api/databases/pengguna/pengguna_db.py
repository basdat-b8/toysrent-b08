from ..DBManager import DBManager


class PenggunaDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO pengguna
                   (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp)
                   VALUES ('{0}','{1}','{2}','{3}','{4}');""".format(
            db_models.no_ktp,
            db_models.nama_lengkap,
            db_models.email,
            db_models.tanggal_lahir,
            db_models.no_telp
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM pengguna;"""
        cursor = super().get_all_data_from_database(database_query)
        from .pengguna_models import PenggunaModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PenggunaModel()
            db_object.no_ktp = db_row[0]
            db_object.nama_lengkap = db_row[1]
            db_object.email = db_row[2]
            db_object.tanggal_lahir = db_row[3]
            db_object.no_telp = db_row[4]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM pengguna
                            WHERE no_ktp='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .pengguna_models import PenggunaModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PenggunaModel()
            db_object.no_ktp = db_row[0]
            db_object.nama_lengkap = db_row[1]
            db_object.email = db_row[2]
            db_object.tanggal_lahir = db_row[3]
            db_object.no_telp = db_row[4]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE pengguna
                            SET no_ktp='{0}',
                            nama_lengkap='{1}',
                            email='{2}',
                            tanggal_lahir='{3}',
                            no_telp='{4}'
                            WHERE no_ktp='{0}';""".format(
            db_models.no_ktp,
            db_models.nama_lengkap,
            db_models.email,
            db_models.tanggal_lahir,
            db_models.no_telp
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM pengguna
                            WHERE no_ktp='{0}';""".format(db_models.no_ktp)
        super().delete_data_from_database(database_query)
        return
