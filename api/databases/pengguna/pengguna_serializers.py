from rest_framework import serializers
from .pengguna_models import PenggunaModel


class PenggunaSerializer(serializers.Serializer):
    no_ktp = serializers.CharField(max_length=20)
    nama_lengkap = serializers.CharField(required=True, allow_blank=False, max_length=255)
    email = serializers.CharField(required=True, allow_blank=False, max_length=255)
    tanggal_lahir = serializers.DateField()
    no_telp = serializers.CharField(required=False, allow_blank=True, max_length=20)

    def create(self, validated_data):
        db_object = PenggunaModel()
        db_object.no_ktp = validated_data.get("no_ktp")
        db_object.nama_lengkap = validated_data.get("nama_lengkap")
        db_object.email = validated_data.get("email")
        db_object.tanggal_lahir = validated_data.get("tanggal_lahir")
        db_object.no_telp = validated_data.get("no_telp")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_ktp = validated_data.get("no_ktp", db_object.no_ktp)
        db_object.nama_lengkap = validated_data.get("nama_lengkap", db_object.nama_lengkap)
        db_object.email = validated_data.get("email", db_object.email)
        db_object.tanggal_lahir = validated_data.get("tanggal_lahir", db_object.tanggal_lahir)
        db_object.no_telp = validated_data.get("no_telp", db_object.no_telp)
        db_object.init_save_data_to_database()
        return db_object
