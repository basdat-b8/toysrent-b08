from .pengguna_db import PenggunaDB
from ..ModelManager import ModelManager


class PenggunaModel(ModelManager):
    db_objects = PenggunaDB()

    def __init__(self):
        self.no_ktp = None
        self.nama_lengkap = None
        self.email = None
        self.tanggal_lahir = None
        self.no_telp = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
