from rest_framework import serializers
from .pengiriman_models import PengirimanModel


class PengirimanSerializer(serializers.Serializer):
    no_resi= serializers.CharField(max_length=10)
    id_pemesanan = serializers.CharField(max_length=10)
    metode = serializers.CharField(required=True, allow_blank=False, max_length=255)
    ongkos = serializers.IntegerField(required=True)
    tanggal = serializers.DateField(required=True)
    no_ktp_anggota = serializers.CharField(max_length=20)
    nama_alamat_anggota = serializers.CharField(max_length=255)

    def create(self, validated_data):
        db_object = PengirimanModel()
        db_object.no_resi = validated_data.get("no_resi")
        db_object.id_pemesanan = validated_data.get("id_pemesanan")
        db_object.metode = validated_data.get("metode")
        db_object.ongkos = validated_data.get("ongkos")
        db_object.tanggal = validated_data.get("tanggal")
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota")
        db_object.nama_alamat_anggota = validated_data.get("nama_alamat_anggota")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.no_resi = validated_data.get("no_resi", db_object.no_resi)
        db_object.id_pemesanan = validated_data.get("id_pemesanan", db_object.id_pemesanan)
        db_object.metode = validated_data.get("metode", db_object.metode)
        db_object.ongkos = validated_data.get("ongkos", db_object.ongkos)
        db_object.tanggal = validated_data.get("tanggal", db_object.tanggal)
        db_object.no_ktp_anggota = validated_data.get("no_ktp_anggota", db_object.no_ktp_anggota)
        db_object.nama_alamat_anggota = validated_data.get("nama_alamat_anggota", db_object.nama_alamat_anggota)
        db_object.init_save_data_to_database()
        return db_object
