from .pengiriman_db import PengirimanDB
from ..ModelManager import ModelManager


class PengirimanModel(ModelManager):
    db_objects = PengirimanDB()

    def __init__(self):
        self.no_resi= None
        self.id_pemesanan= None
        self.metode = None
        self.ongkos = None
        self.tanggal= None
        self.no_ktp_anggota = None
        self.nama_alamat_anggota = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
