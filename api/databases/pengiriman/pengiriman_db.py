from ..DBManager import DBManager


class PengirimanDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO pengiriman
                   (no_resi,id_pemesanan,metode,ongkos,tanggal,no_ktp_anggota,nama_alamat_anggota)
                   VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}');""".format(
            db_models.no_resi,
            db_models.id_pemesanan,
            db_models.metode,
            db_models.ongkos,
            db_models.tanggal,
            db_models.no_ktp_anggota,
            db_models.nama_alamat_anggota
        )
        print(database_query)
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM pengiriman;"""
        cursor = super().get_all_data_from_database(database_query)
        from .pengiriman_models import PengirimanModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PengirimanModel()
            db_object.no_resi= db_row[0]
            db_object.id_pemesanan= db_row[1]
            db_object.metode= db_row[2]
            db_object.ongkos= db_row[3]
            db_object.tanggal= db_row[4]
            db_object.no_ktp_anggota = db_row[5]
            db_object.nama_alamat_anggota = db_row[6]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM pengiriman
                            WHERE no_resi='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .pengiriman_models import PengirimanModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = PengirimanModel()
            db_object.no_resi= db_row[0]
            db_object.id_pemesanan= db_row[1]
            db_object.metode= db_row[2]
            db_object.ongkos= db_row[3]
            db_object.tanggal= db_row[4]
            db_object.no_ktp_anggota = db_row[5]
            db_object.nama_alamat_anggota = db_row[6]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE pengiriman
                            SET no_resi='{0}',
                            id_pemesanan='{1}',
                            metode='{2}',
                            ongkos='{3}',
                            tanggal='{4}',
                            no_ktp_anggota='{5}',
                            nama_alamat_anggota='{6}'
                            WHERE no_resi='{0}';""".format(
            db_models.no_resi,
            db_models.id_pemesanan,
            db_models.metode,
            db_models.ongkos,
            db_models.tanggal,
            db_models.no_ktp_anggota,
            db_models.nama_alamat_anggota
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM pengiriman
                            WHERE no_resi='{0}';""".format(db_models.no_resi)
        super().delete_data_from_database(database_query)
        return
