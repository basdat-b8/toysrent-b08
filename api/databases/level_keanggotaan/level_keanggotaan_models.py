from .level_keanggotaan_db import LevelKeanggotaanDB
from ..ModelManager import ModelManager


class LevelKeanggotaanModel(ModelManager):
    db_objects = LevelKeanggotaanDB()

    def __init__(self):
        self.nama_level = None
        self.mininum_poin = None
        self.deskripsi = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
