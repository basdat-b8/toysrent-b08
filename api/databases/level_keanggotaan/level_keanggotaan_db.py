from ..DBManager import DBManager


class LevelKeanggotaanDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO level_keanggotaan
                   (nama_level,minimum_poin,deskripsi)
                   VALUES ('{0}','{1}','{2}');""".format(
            db_models.nama_level,
            db_models.minimum_poin,
            db_models.deskripsi)
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM level_keanggotaan;"""
        cursor = super().get_all_data_from_database(database_query)
        from .level_keanggotaan_models import LevelKeanggotaanModel
        list_of_db_objects = []
        for db_row in cursor.fetchall():
            db_object = LevelKeanggotaanModel()
            db_object.nama_level = db_row[0]
            db_object.minimum_poin = db_row[1]
            db_object.deskripsi = db_row[2]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT nama_level,
                            minimum_poin,
                            deskripsi
                            FROM level_keanggotaan
                            WHERE nama_level='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .level_keanggotaan_models import LevelKeanggotaanModel
        list_of_db_objects = []
        for db_row in cursor.fetchall():
            db_object = LevelKeanggotaanModel()
            db_object.nama_level = db_row[0]
            db_object.minimum_poin = db_row[1]
            db_object.deskripsi = db_row[2]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE level_keanggotaan
                            SET nama_level='{0}',
                            minimum_poin='{1}',
                            deskripsi='{2}'
                            WHERE nama_level='{0}';""".format(
            db_models.nama_level,
            db_models.minimum_poin,
            db_models.deskripsi)
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM level_keanggotaan
                            WHERE nama_level='{0}';""".format(db_models.nama_level)
        super().delete_data_from_database(database_query)
        return
