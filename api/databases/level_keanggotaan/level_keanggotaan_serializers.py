from rest_framework import serializers
from .level_keanggotaan_models import LevelKeanggotaanModel


class LevelKeanggotaanSerializer(serializers.Serializer):
    nama_level = serializers.CharField(max_length=20)
    minimum_poin = serializers.IntegerField(required=True)
    deskripsi = serializers.CharField(required=False, allow_blank=True, max_length=128)

    def create(self, validated_data):
        db_object = LevelKeanggotaanModel()
        db_object.nama_level = validated_data.get("nama_level")
        db_object.minimum_poin = validated_data.get("minimum_poin")
        db_object.deskripsi = validated_data.get("deskripsi")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.nama_level = validated_data.get("nama_level", db_object.nama_level)
        db_object.minimum_poin = validated_data.get("minimum_poin", db_object.minimum_poin)
        db_object.deskripsi = validated_data.get("deskripsi", db_object.deskripsi)
        db_object.init_save_data_to_database()
        return db_object
