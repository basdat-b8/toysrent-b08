from rest_framework import serializers
from .item_models import ItemModel


class ItemSerializer(serializers.Serializer):
    nama = serializers.CharField(max_length=255)
    deskripsi = serializers.CharField(required=False, allow_blank=True, max_length=255)
    usia_dari = serializers.IntegerField(required=True)
    usia_sampai = serializers.IntegerField(required=True)
    bahan = serializers.CharField(required=False, allow_blank=True, max_length=255)

    def create(self, validated_data):
        db_object = ItemModel()
        db_object.nama = validated_data.get("nama")
        db_object.deskripsi = validated_data.get("deskripsi")
        db_object.usia_dari = validated_data.get("usia_dari")
        db_object.usia_sampai = validated_data.get("usia_sampai")
        db_object.bahan = validated_data.get("bahan")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.nama = validated_data.get("nama", db_object.nama)
        db_object.deskripsi = validated_data.get("deskripsi", db_object.deskripsi)
        db_object.usia_dari = validated_data.get("usia_dari", db_object.usia_dari)
        db_object.usia_sampai = validated_data.get("usia_sampai", db_object.usia_sampai)
        db_object.bahan = validated_data.get("bahan", db_object.bahan)

        db_object.init_save_data_to_database()
        return db_object
