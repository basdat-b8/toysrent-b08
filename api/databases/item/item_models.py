from .item_db import ItemDB
from ..ModelManager import ModelManager


class ItemModel(ModelManager):
    db_objects = ItemDB()

    def __init__(self):
        self.nama = None
        self.deskripsi = None
        self.usia_dari = None
        self.usia_sampai = None
        self.bahan = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
