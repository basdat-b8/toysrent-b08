from ..DBManager import DBManager


class ItemDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO item
                   (nama,deskripsi,usia_dari,usia_sampai,bahan)
                   VALUES ('{0}','{1}','{2}','{3}','{4}');""".format(
            db_models.nama,
            db_models.deskripsi,
            db_models.usia_dari,
            db_models.usia_sampai,
            db_models.bahan
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM item;"""
        cursor = super().get_all_data_from_database(database_query)
        from .item_models import ItemModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = ItemModel()
            db_object.nama = db_row[0]
            db_object.deskripsi = db_row[1]
            db_object.usia_dari = db_row[2]
            db_object.usia_sampai = db_row[3]
            db_object.bahan = db_row[4]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM item
                            WHERE nama='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .item_models import ItemModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = ItemModel()
            db_object.nama = db_row[0]
            db_object.deskripsi = db_row[1]
            db_object.usia_dari = db_row[2]
            db_object.usia_sampai = db_row[3]
            db_object.bahan = db_row[4]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE item
                            SET nama='{0}',
                            deskripsi='{1}',
                            usia_dari='{2}',
                            usia_sampai='{3}',
                            bahan='{4}'
                            WHERE nama='{0}';""".format(
            db_models.nama,
            db_models.deskripsi,
            db_models.usia_dari,
            db_models.usia_sampai,
            db_models.bahan
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM item
                            WHERE nama='{0}';""".format(db_models.nama)
        super().delete_data_from_database(database_query)
        return
