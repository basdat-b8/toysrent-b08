from ..DBManager import DBManager


class KategoriItemDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO kategori_item
                   (nama_item,nama_kategori)
                   VALUES ('{0}','{1}');""".format(
            db_models.nama_item,
            db_models.nama_kategori
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM kategori_item;"""
        cursor = super().get_all_data_from_database(database_query)
        from .kategori_item_models import KategoriItemModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = KategoriItemModel()
            db_object.nama_item = db_row[0]
            db_object.nama_kategori = db_row[1]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM kategori_item
                            WHERE nama_item='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .kategori_item_models import KategoriItemModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = KategoriItemModel()
            db_object.nama_item = db_row[0]
            db_object.nama_kategori = db_row[1]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects

    def update_data_to_database(self, db_models):
        database_query = """UPDATE kategori_item
                            SET nama_item='{0}',
                            nama_kategori='{1}'
                            WHERE nama_item='{0}'
                            AND nama_kategori='{1}';""".format(
            db_models.nama_item,
            db_models.nama_kategori
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM kategori_item
                            WHERE nama_item='{0}'
                            AND nama_kategori='{1}';""".format(db_models.nama_item, db_models.nama_kategori)
        super().delete_data_from_database(database_query)
        return
