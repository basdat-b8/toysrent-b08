from .kategori_item_db import KategoriItemDB
from ..ModelManager import ModelManager


class KategoriItemModel(ModelManager):
    db_objects = KategoriItemDB()

    def __init__(self):
        self.nama_item = None
        self.nama_kategori = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
