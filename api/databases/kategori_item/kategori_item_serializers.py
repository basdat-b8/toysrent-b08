from rest_framework import serializers
from .kategori_item_models import KategoriItemModel


class KategoriItemSerializer(serializers.Serializer):
    nama_item = serializers.CharField(max_length=255)
    nama_kategori = serializers.CharField(max_length=255)

    def create(self, validated_data):
        db_object = KategoriItemModel()
        db_object.nama_item = validated_data.get("nama_item")
        db_object.nama_kategori = validated_data.get("nama_kategori")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.nama_item = validated_data.get("nama_item", db_object.nama_item)
        db_object.nama_kategori = validated_data.get("nama_kategori", db_object.nama_kategori)
        db_object.init_save_data_to_database()
        return db_object
