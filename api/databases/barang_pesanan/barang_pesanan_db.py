from ..DBManager import DBManager


class BarangPesananDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO barang_pesanan
                   (id_pemesanan,no_urut,id_barang,tanggal_sewa,lama_sewa,tanggal_kembali,status)
                   VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}');""".format(
            db_models.id_pemesanan,
            db_models.no_urut,
            db_models.id_barang,
            db_models.tanggal_sewa,
            db_models.lama_sewa,
            db_models.tanggal_kembali,
            db_models.status
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM barang_pesanan;"""
        cursor = super().get_all_data_from_database(database_query)
        from .barang_pesanan_models import BarangPesananModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangPesananModel()
            db_object.id_pemesanan = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            db_object.tanggal_sewa = db_row[3]
            db_object.lama_sewa = db_row[4]
            db_object.tanggal_kembali = db_row[5]
            db_object.status = db_row[6]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM barang_pesanan
                            WHERE id_pemesanan='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .barang_pesanan_models import BarangPesananModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangPesananModel()
            db_object.id_pemesanan = db_row[0]
            db_object.no_urut = db_row[1]
            db_object.id_barang = db_row[2]
            db_object.tanggal_sewa = db_row[3]
            db_object.lama_sewa = db_row[4]
            db_object.tanggal_kembali = db_row[5]
            db_object.status = db_row[6]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects

    def update_data_to_database(self, db_models):
        database_query = """UPDATE barang_pesanan
                            SET id_pemesanan='{0}',
                            no_urut='{1}',
                            id_barang='{2}',
                            tanggal_sewa='{3}',
                            lama_sewa='{4}',
                            tangga_kembali='{5}',
                            status='{6}'
                            WHERE id_pemesanan='{0}'
                            AND no_urut='{1}';""".format(
            db_models.id_pemesanan,
            db_models.no_urut,
            db_models.id_barang,
            db_models.tanggal_sewa,
            db_models.lama_sewa,
            db_models.tanggal_kembali,
            db_models.status
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM barang_pesanan
                            WHERE id_pemesanan='{0}'
                            AND no_urut='{1}';""".format(db_models.id_pemesanan, db_models.no_urut)
        super().delete_data_from_database(database_query)
        return
