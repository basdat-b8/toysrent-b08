from .barang_pesanan_db import BarangPesananDB
from ..ModelManager import ModelManager


class BarangPesananModel(ModelManager):
    db_objects = BarangPesananDB()

    def __init__(self):
        self.id_pemesanan = None
        self.no_urut = None
        self.id_barang = None
        self.tanggal_sewa = None
        self.lama_sewa = None
        self.tanggal_sewa = None
        self.status = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
