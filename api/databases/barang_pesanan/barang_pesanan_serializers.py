from rest_framework import serializers
from .barang_pesanan_models import BarangPesananModel


class BarangPesananSerializer(serializers.Serializer):
    id_pemesanan = serializers.CharField(max_length=10)
    no_urut = serializers.CharField(max_length=10)
    id_barang = serializers.CharField(max_length=10)
    tanggal_sewa = serializers.DateField(required=True)
    lama_sewa = serializers.IntegerField(required=True)
    tanggal_kembali = serializers.DateField()
    status = serializers.CharField(required=True, max_length=50)

    def create(self, validated_data):
        db_object = BarangPesananModel()
        db_object.id_pemesanan = validated_data.get("id_pemesanan")
        db_object.no_urut = validated_data.get("no_urut")
        db_object.id_barang = validated_data.get("id_barang")
        db_object.tanggal_sewa = validated_data.get("tanggal_sewa")
        db_object.lama_sewa = validated_data.get("lama_sewa")
        db_object.tanggal_kembali = validated_data.get("tanggal_kembali")
        db_object.status = validated_data.get("status")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.id_pemesanan = validated_data.get("id_pemesanan", db_object.id_pemesanan)
        db_object.no_urut = validated_data.get("no_urut", db_object.no_urut)
        db_object.id_barang = validated_data.get("id_barang", db_object.id_barang)
        db_object.tanggal_sewa = validated_data.get("tanggal_sewa", db_object.tanggal_sewa)
        db_object.lama_sewa = validated_data.get("lama_sewa", db_object.lama_sewa)
        db_object.tanggal_kembali = validated_data.get("tanggal_kembali", db_object.tanggal_kembali)
        db_object.status = validated_data.get("status", db_object.status)
        db_object.init_save_data_to_database()
        return db_object
