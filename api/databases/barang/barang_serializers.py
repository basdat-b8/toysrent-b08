from rest_framework import serializers
from .barang_models import BarangModel


class BarangSerializer(serializers.Serializer):
    id_barang = serializers.CharField(max_length=10)
    nama_item = serializers.CharField(required=True, allow_blank=False, max_length=255)
    warna = serializers.CharField(max_length=50)
    url_foto = serializers.CharField(max_length=255)
    kondisi = serializers.CharField(required=True, allow_blank=False, max_length=255)
    lama_penggunaan = serializers.IntegerField()
    no_ktp_penyewa = serializers.CharField(max_length=20)


    def create(self, validated_data):
        db_object = BarangModel()
        db_object.id_barang = validated_data.get("id_barang")
        db_object.nama_item = validated_data.get("nama_item")
        db_object.warna = validated_data.get("warna")
        db_object.url_foto = validated_data.get("url_foto")
        db_object.kondisi = validated_data.get("kondisi")
        db_object.lama_penggunaan = validated_data.get("lama_penggunaan")
        db_object.no_ktp_penyewa = validated_data.get("no_ktp_penyewa")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.id_barang = validated_data.get("id_barang", db_object.no_ktp)
        db_object.nama_item = validated_data.get("nama_item", db_object.poin)
        db_object.warna = validated_data.get("warna", db_object.level)
        db_object.url_foto = validated_data.get("url_foto", db_object.no_ktp)
        db_object.kondisi = validated_data.get("kondisi", db_object.poin)
        db_object.lama_penggunaan = validated_data.get("lama_penggunaan", db_object.level)
        db_object.no_ktp_penyewa = validated_data.get("no_ktp_penyewa", db_object.no_ktp)
        db_object.init_save_data_to_database()
        return db_object
