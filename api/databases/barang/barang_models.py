from .barang_db import BarangDB
from ..ModelManager import ModelManager


class BarangModel(ModelManager):
    db_objects = BarangDB()

    def __init__(self):
        self.id_barang = None
        self.nama_item = None
        self.warna = None
        self.url_foto = None
        self.kondisi = None
        self.lama_penggunaan = None
        self.no_ktp_penyewa = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
