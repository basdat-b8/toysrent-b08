from ..DBManager import DBManager


class BarangDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO barang
                   (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa)
                   VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}');""".format(
            db_models.id_barang,
            db_models.nama_item,
            db_models.warna,
            db_models.url_foto,
            db_models.kondisi,
            db_models.lama_penggunaan,
            db_models.no_ktp_penyewa
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM barang;"""
        cursor = super().get_all_data_from_database(database_query)
        from .barang_models import BarangModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangModel()
            db_object.id_barang = db_row[0]
            db_object.nama_item = db_row[1]
            db_object.warna = db_row[2]
            db_object.url_foto = db_row[3]
            db_object.kondisi = db_row[4]
            db_object.lama_penggunaan = db_row[5]
            db_object.no_ktp_penyewa = db_row[6]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM barang
                            WHERE id_barang='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .barang_models import BarangModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = BarangModel()
            db_object.id_barang = db_row[0]
            db_object.nama_item = db_row[1]
            db_object.warna = db_row[2]
            db_object.url_foto = db_row[3]
            db_object.kondisi = db_row[4]
            db_object.lama_penggunaan = db_row[5]
            db_object.no_ktp_penyewa = db_row[6]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects[0]

    def update_data_to_database(self, db_models):
        database_query = """UPDATE barang
                            SET id_barang='{0}',
                            nama_item='{1}',
                            warna='{2}',
                            url_foto='{3}',
                            kondisi='{4}',
                            lama_penggunaan='{5}',
                            no_ktp_penyewa='{6}'
                            WHERE id_barang='{0}';""".format(
            db_models.id_barang,
            db_models.nama_item,
            db_models.warna,
            db_models.url_foto,
            db_models.kondisi,
            db_models.lama_penggunaan,
            db_models.no_ktp_penyewa
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM barang
                            WHERE id_barang='{0}';""".format(db_models.id_barang)
        super().delete_data_from_database(database_query)
        return
