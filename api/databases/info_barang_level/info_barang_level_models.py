from .info_barang_level_db import InfoBarangLevelDB
from ..ModelManager import ModelManager


class InfoBarangLevelModel(ModelManager):
    db_objects = InfoBarangLevelDB()

    def __init__(self):
        self.id_barang = None
        self.nama_level = None
        self.harga_sewa = None
        self.porsi_royalti = None

    def init_save_data_to_database(self):
        super().init_save_data_to_database(self.db_objects)
        return

    def init_delete_data_from_database(self):
        super().init_delete_data_from_database(self.db_objects)
        return
