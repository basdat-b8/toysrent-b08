from ..DBManager import DBManager


class InfoBarangLevelDB(DBManager):
    def __init__(self):
        pass

    def insert_data_to_database(self, db_models):
        database_query = """INSERT INTO info_barang_level
                   (id_barang,nama_level,harga_sewa,porsi_royalti)
                   VALUES ('{0}','{1}','{2}','{3}');""".format(
            db_models.id_barang,
            db_models.nama_level,
            db_models.harga_sewa,
            db_models.porsi_royalti
        )
        super().insert_data_to_database(database_query)
        return

    def get_all_data_from_database(self):
        database_query = """SELECT * FROM info_barang_level;"""
        cursor = super().get_all_data_from_database(database_query)
        from .info_barang_level_models import InfoBarangLevelModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = InfoBarangLevelModel()
            db_object.id_barang = db_row[0]
            db_object.nama_level = db_row[1]
            db_object.harga_sewa = db_row[2]
            db_object.porsi_royalti = db_row[3]
            list_of_db_objects.append(db_object)
        return list_of_db_objects

    def get_data_from_database_by_id(self, id_to_find):
        database_query = """SELECT *
                            FROM info_barang_level
                            WHERE id_barang='{0}';""".format(id_to_find)
        cursor = super().get_data_from_database_by_id(database_query)
        from .info_barang_level_models import InfoBarangLevelModel
        list_of_db_objects = []

        for db_row in cursor.fetchall():
            db_object = InfoBarangLevelModel()
            db_object.id_barang = db_row[0]
            db_object.nama_level = db_row[1]
            db_object.harga_sewa = db_row[2]
            db_object.porsi_royalti = db_row[3]
            list_of_db_objects.append(db_object)
        return None if len(list_of_db_objects) == 0 else list_of_db_objects

    def update_data_to_database(self, db_models):
        database_query = """UPDATE info_barang_level
                            SET id_barang='{0}',
                            nama_level='{1}',
                            harga_sewa='{2}',
                            porsi_royalti='{3}'
                            WHERE id_barang='{0}'
                            AND nama_level='{1}';""".format(
            db_models.id_barang,
            db_models.nama_level,
            db_models.harga_sewa,
            db_models.porsi_royalti
        )
        super().update_data_to_database(database_query)
        return

    def delete_data_from_database(self, db_models):
        database_query = """DELETE FROM info_barang_level
                            WHERE id_barang='{0}'
                            AND nama_level='{1}';""".format(db_models.id_barang, db_models.nama_level)
        super().delete_data_from_database(database_query)
        return
