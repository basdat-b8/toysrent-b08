from rest_framework import serializers
from .info_barang_level_models import InfoBarangLevelModel


class InfoBarangLevelSerializer(serializers.Serializer):
    id_barang = serializers.CharField(max_length=10)
    nama_level = serializers.CharField(max_length=10)
    harga_sewa = serializers.IntegerField(required=True)
    porsi_royalti = serializers.FloatField(required=True)

    def create(self, validated_data):
        db_object = InfoBarangLevelModel()
        db_object.id_barang = validated_data.get("id_barang")
        db_object.nama_level = validated_data.get("nama_level")
        db_object.harga_sewa = validated_data.get("harga_sewa")
        db_object.porsi_royalti = validated_data.get("porsi_royalti")
        db_object.init_save_data_to_database()
        return db_object

    def update(self, db_object, validated_data):
        db_object.id_barang = validated_data.get("id_barang", db_object.id_barang)
        db_object.nama_level = validated_data.get("nama_level", db_object.nama_level)
        db_object.harga_sewa = validated_data.get("harga_sewa", db_object.harga_sewa)
        db_object.porsi_royalti = validated_data.get("porsi_royalti", db_object.porsi_royalti)
        db_object.init_save_data_to_database()
        return db_object
