import json
import requests

from django.views import View
from django.shortcuts import render

class APIPage(View):
    def get(self, request):
        page_file = "api.html"
        return render(request, page_file)