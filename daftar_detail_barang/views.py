from django.shortcuts import render

# Create your views here.
def detail_page(request):
    return render(request, "detail.html")

def gundam(request):
    return render(request, "gundam.html")