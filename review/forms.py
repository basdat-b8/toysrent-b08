from django import forms

ITEMS_CHOICES= [
    ('A1', 'A1'),
    ('A2', 'A2'),
]

class DeliveryRegistrationForm(forms.Form):
    barang_pesanan = forms.CharField(label='Ordered Items', widget=forms.SelectMultiple(choices=ITEMS_CHOICES))
    review = forms.CharField(max_length=255, required=True, label='review')