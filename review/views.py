import json
import requests

from django.views import View
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
from review.forms import DeliveryRegistrationForm

# Create your views here.

# Create your views here.
def daftar_pengiriman(request):
    response = {}
    response['activetab'] = 'daftarreview'
    page = "list_review.html"
    data = getReviewData()
    context = {
        "data" : data
    }
    return render(request, page, context)

def getReviewData():
    # ini link apinya ubah
    json_req = requests.get("http://localhost:8000/api/products/delivery/all/")
    json_req_2 = requests.get("http://localhost:8000/api/products/deliveries/all/")
    json_data = json_req.json()
    return json_data

#Untuk Delivery Form
def daftar_pengiriman_add(request):
    page_file = "pengiriman_add.html"
    context = {
        "delivery_registration_form" : DeliveryRegistrationForm
    }
    return render(request, page_file, context)

@csrf_exempt       
def additemaction(request):  
    data_to_send = {
        "barang_pesanan" : request.POST['barang_pesanan'],
        "review" : request.POST['review'],
    }
    requests.post("http://localhost:8000/api/delivery/register/", data=data_to_send)
    data = { "item_added" : 1 }
    return JsonResponse(data)
    
@csrf_exempt
def deleteitemaction(request, no_resi):
    requests.delete("http://localhost:8000/api/delivery/" + no_resi)
    return redirect('/reviews/')