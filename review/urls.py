from django.urls import path
from review import views

urlpatterns = [
    path('', views.daftar_pengiriman, name='review'),
    path('add/', views.daftar_pengiriman_add, name="review_add"),
    path('delete/<str:no_resi>/', views.deleteitemaction, name="delete_review_action"),
]