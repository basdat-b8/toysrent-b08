from django import forms

class Pesanan(forms.Form):
    Anggota = forms.CharField(label='Anggota', required=True,
        max_length=50, widget=forms.ChoiceField())
    Barang = forms.CharField(label='Barang', required=True,
        max_length=50, widget=forms.ChoiceField())
    LamaSewa = forms.IntegerField(label='Lama Sewa', required=True,
        max_length=50, widget=forms.ChoiceField())