from django.shortcuts import render

# Create your views here.

def pesanan(request):
    response = {}
    response['activetab'] = 'pesanan'
    return render(request, "pesanan.html", response)

def daftar_pesanan(request):
    response = {}
    response['activetab'] = 'daftarpesanan'
    return render(request, "daftar_pesanan.html", response)

def updatepesanan(request):
    response = {}
    return render(request,"formupdatepesanan.html", response)